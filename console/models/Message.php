<?php
namespace console\models;

use Yii;
use yii\base\Model;

class Message extends Model {

	public $phoneNumber;
	public $messageText = "";

	/**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['phoneNumber', 'messageText'], 'required']
        ];
    }

    public function toString() {
    	return $this->phoneNumber."\n".$this->messageText;
    }
}
