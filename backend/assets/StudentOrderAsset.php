<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Arslan Imamutdinov <a.imamutdinov@its-spc.ru>
 * @since 2.0
 */
class StudentOrderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [
        'js/angular.min.js',
        'js/student.order.app.js'
    ];
    public $depends = [];
}