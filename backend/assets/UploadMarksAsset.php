<?php

namespace backend\assets;

use yii\web\AssetBundle;

class UploadMarksAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [
        'js/angular.min.js',
        'js/upload.marks.app.js'
    ];
    public $depends = [];
}
