<?php

namespace app\modules\worker\models;

use yii\base\Model;
use Yii;
use Yii\web\UploadedFile;


/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property string $name
 */
class UploadMarks extends Model {

    public $schoolYearId;
    public $schoolId;
    public $schoolClassId;
    public $subjectId;
    public $uploadFile;

    public $schoolYears;
    public $subjects;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['schoolYearId', 'schoolId', 'schoolClassId', 'subjectId'], 'required'],
            [['schoolYearId', 'schoolId', 'schoolClassId', 'subjectId'], 'integer', 'message' => 'Поле обязательное для заполнения'],
            [['uploadFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'txt'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'schoolYearId' => 'Учебный год',
            'schoolId' => 'Школа',
			'schoolClassId' => 'Класс',
			'subjectId' => 'Предмет',
            'uploadFile' => 'Файл с оценками'
        ];
    }
}
