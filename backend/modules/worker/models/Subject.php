<?php

namespace app\modules\worker\models;

use Yii;

/**
 * This is the model class for table "subject".
 *
 * @property integer $id
 * @property string $name
 *
 * @property StudentMark[] $studentMarks
 * @property StudentSubject[] $studentSubjects
 * @property SchoolStudent[] $students
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название предмета',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentMarks()
    {
        return $this->hasMany(StudentMark::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentSubjects()
    {
        return $this->hasMany(StudentSubject::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(SchoolStudent::className(), ['id' => 'student_id'])->viaTable('student_subject', ['subject_id' => 'id']);
    }
}
