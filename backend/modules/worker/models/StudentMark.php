<?php

namespace app\modules\worker\models;

use Yii;

/**
 * This is the model class for table "student_mark".
 *
 * @property integer $id
 * @property integer $subject_id
 * @property integer $student_id
 * @property string $mark_date
 * @property string $mark
 *
 * @property Subject $subject
 * @property SchoolStudent $student
 */
class StudentMark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_mark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id', 'student_id', 'mark_date', 'mark'], 'required'],
            [['subject_id', 'student_id'], 'integer'],
            [['mark_date'], 'safe'],
            [['mark'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject_id' => 'Subject ID',
            'student_id' => 'Student ID',
            'mark_date' => 'Mark Date',
            'mark' => 'Mark',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(SchoolStudent::className(), ['id' => 'student_id']);
    }
}
