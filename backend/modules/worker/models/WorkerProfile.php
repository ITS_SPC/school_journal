<?php

namespace app\modules\worker\models;

use Yii;
use app\modules\worker\models\User;

/**
 * This is the model class for table "worker".
 *
 * @property integer $id
 * @property string $surname
 * @property string $name
 * @property string $lastname
 * @property integer $user_id
 *
 * @property User $id0
 */
class WorkerProfile extends \yii\db\ActiveRecord
{

    public $user;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'surname', 'name', 'lastname', 'user_id'], 'required'],
            [['id', 'user_id'], 'integer'],
            [['surname', 'name', 'lastname'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'lastname' => 'Отчество',
            'user_id' => 'Пользователь',
            'login' => 'Логин'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
}
