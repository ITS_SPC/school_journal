<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\administrator\models\Subject;
use app\modules\administrator\models\SchoolStudent;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\StudentMark */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-mark-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mark_date')->widget(DatePicker::className(),[
		'language' => 'ru',
		'clientOptions'=>[
			'format'=>'yyyy-mm-dd']
		]) ?>
	
    <?= $form->field($model, 'mark')->textInput(['maxlength' => true]) ?>
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>