<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Успеваемость';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="panel panel-primary">
  		<div class="panel-heading">Загрузка оценок для класса по предмету</div>
  		<div class="panel-body">
  			<?= $this->render('_form', [
        		'model' => $model
    		]) ?>
  		</div>
	</div>

</div>
