<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\ScoreTableWidget;
use yii\helpers\ArrayHelper;
use backend\assets\UploadMarksAsset;
use kartik\file\FileInput;


UploadMarksAsset::register($this);
/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div ng-app="uploadMarksApp" class="parent-profile-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div ng-controller="UploadForm">
        <div class="row">
            <div class="col-md-3">
            
                <?= $form->field($model, 'schoolYearId')->dropdownList(ArrayHelper::map($model->schoolYears, 'id',
                            function($model, $defaultValue) {
                                return $model->start_year.' - '.$model->end_year;
                            }), ['prompt' => '---Учебный год---','ng-model' => 'schoolYearId']) ?>

            </div>
            <div class="col-md-3">
            
                <?= $form->field($model, 'schoolId')->dropdownList(['prompt' => '---Школа---'], ['ng-model' => 'selectedSchool.id', 'ng-options' => 'school.id as school.name for school in schools track by school.id', ]) ?>

            </div>
            <div class="col-md-3">
            
                <?= $form->field($model, 'schoolClassId')->dropdownList(['prompt' => '---Класс---'], ['ng-model' => 'selectedClass.id', 'ng-options' => 'class.id as class.name for class in schoolClasses track by class.id', ]) ?>

            </div>
            <div class="col-md-3">
            
                <?= $form->field($model, 'subjectId')->dropdownList(ArrayHelper::map($model->subjects, 'id',
                            function($model, $defaultValue) {
                                return $model->name;
                            }), ['prompt' => '---Предмет---']) ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-4">

                <?= $form->field($model, 'uploadFile')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'text/*'],
                ]); ?>

            </div>
        </div>
    </div>
    <div class="row text-left">
        <div class="col-md-1">
            <?= Html::submitButton('Загрузить', ['class' => 'btn btn-success' ]) ?>
        </div>
        <div class="col-md-3">
            <span class="spinner" style="display:none"></span>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
