<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мой профиль';
?>
<div class="worker-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('view', [
    	'profile_model' => $profile_model,
    ]) ?>

</div>