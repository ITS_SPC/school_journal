<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */
?>
<div class="parent-profile-view">

    <?= DetailView::widget([
        'model' => $profile_model,
        'attributes' => [
            'surname',
            'name',
            'lastname',
            [
            	'attribute' => 'user.username',
            	'label' => 'Логин'
            ],
            [
            	'attribute' => 'user.email',
            	'label' => 'E-mail'
            ],
            [
                'attribute' => 'phone',
                'label' => 'Телефон'
            ],
        ],
    ]) ?>

</div>