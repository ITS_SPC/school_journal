<?php

namespace app\modules\worker;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\worker\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
