<?php

namespace app\modules\worker\controllers;

use Yii;
use app\modules\administrator\models\StudentMark;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SchoolStudent;
use app\modules\administrator\models\StudentMarkSearch;
use app\modules\administrator\models\StudentFilterForm;
use app\modules\administrator\models\School;
use app\modules\administrator\models\Subject;
use app\modules\administrator\models\SchoolClass;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\worker\controllers\DefaultController;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;


/**
 * StudentMarkController implements the CRUD actions for StudentMark model.
 */
class StudentMarkController extends DefaultController
{

   /* public function actionIndex()
    {
        $studentFilterForm = new StudentFilterForm();
        $studentFilterForm->school_years = SchoolYear::find()->all();
        $studentFilterForm->subjectAll = Subject::find()->all();
        $studentFilterForm->subject=$_POST[StudentFilterForm][subject];
        if($studentFilterForm->load(\Yii::$app->request->post())) {
            $studentFilterForm->getStudentsOfClass();
            return $this->render('index', [
                'studentFilterForm' => $studentFilterForm,
            ]);
        }else {
            return $this->render('index', [
                'studentFilterForm' => $studentFilterForm,
            ]);
        }
    }*/

    public function actionIndex($school_year_id=null,$school_id=null,$school_class_id=null)
    {
        $studentFilterForm = new StudentFilterForm();

        if(($_POST[StudentFilterForm][school_year_id]!=null)&&
                ($_POST[StudentFilterForm][school_id]!=null)&&
                ($_POST[StudentFilterForm][school_class_id]!=null)) 
            {
                $studentFilterForm->school_years = SchoolYear::find()->all();
                if($studentFilterForm->load(\Yii::$app->request->post())) {
                    $studentFilterForm->getStudentsOfClass();
                    return $this->render('index', [
                    'studentFilterForm' => $studentFilterForm,
                    ]);
                }else{
                    return $this->render('index', [
                        'studentFilterForm' => $studentFilterForm,
                    ]);
                }
            }elseif(($school_year_id!=null)&&($school_id!=null)&&($school_class_id!=null)){
                $studentFilterForm->school_year_id=$school_year_id;
                $studentFilterForm->school_id=$school_id;
                $studentFilterForm->school_class_id=$school_class_id;
                $studentFilterForm->school_years = SchoolYear::find()->all();
                $studentFilterForm->getStudentsOfClass();
                return $this->render('index', [
                        'studentFilterForm' => $studentFilterForm,
                    ]);
            }else{
                $studentFilterForm->school_years = SchoolYear::find()->all();
                if($studentFilterForm->load(\Yii::$app->request->post())) {
                    $studentFilterForm->getStudentsOfClass();
                    return $this->render('index', [
                        'studentFilterForm' => $studentFilterForm,
                    ]);
                 }else{
                     return $this->render('index', [
                        'studentFilterForm' => $studentFilterForm,
                    ]);
                }
            }
    }   



    /**
     * Displays a single StudentMark model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id,$subject_id=null)
    {
        if(isset($_GET[Subject][id])){
            $id_of_subject=$_GET[Subject][id];
        }else{
            $id_of_subject=$subject_id;
        }

        $subject = new Subject();
        if($id_of_subject==null){
            return $this->render('view', [
                'subject'=> $subject,
                'student' => $this->findStudentById($id),
            ]);
        }else{
           // $dataProvider = $this->getStudentMarks($id, $_POST[Subject][id]);
            $dataProvider = new ActiveDataProvider([
                'query' => StudentMark::find()->where(['student_id'=> $id,'subject_id'=>$id_of_subject]),
                'pagination' => [
                   'pageSize' => 20,
                    ],
                ]);

            $model=$dataProvider->getModels();

            return $this->render('view', [
                'dataProvider' => $dataProvider,
                'subject'=> $subject,
                'student' => $this->findStudentById($id),
                'model' => $model,
            ]);
        }
    }

    /*protected function getStudentMarks($student_id, $subject_id)
    {
       $dataProvider = new ActiveDataProvider([
                'query' => StudentMark::find()
                        ->where(['student_id'=> $student_id,'subject_id'=>$subject_id])
                    ]);    
        return  $dataProvider;
    }*/

    /**
     * Creates a new StudentMark model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($student_id, $subject_id)
    {
        $model = new StudentMark();
        $model->student_id=$student_id;
        $model->subject_id=$subject_id;

        if ($model->load(Yii::$app->request->post())) {
            $studentMark = StudentMark::getMarkByStudentSubjectDate($model->student_id, $model->subject_id, $model->mark_date);
            if ($studentMark) {
                $studentMark->mark = $model->mark;
                $studentMark->save();
            } else {
                $model->save();
            }
            return $this->redirect(['view','id'=>$student_id, 'subject_id'=>$model->subject_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    } 

    /**
     * Updates an existing StudentMark model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $studentMark = StudentMark::getMarkByStudentSubjectDate($model->student_id, $model->subject_id, $model->mark_date);
            if ($studentMark) {
                $studentMark->mark = $model->mark;
                $studentMark->save();
            } else {
                $model->save();
            }
            return $this->redirect(['view','id'=> $model->student_id, 'subject_id'=>$model->subject_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    } 

    /**
     * Deletes an existing StudentMark model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $student_id=$model->student_id;
        $subject_id=$model->subject_id;
        $model->delete();

        return $this->redirect(['view','id'=> $student_id, 'subject_id'=>$subject_id]);
    }

    /**
     * Finds the StudentMark model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentMark the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentMark::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetClasses() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId && $params->data->schoolId) {
            $schoolClasses = SchoolClass::find()
                ->join('JOIN', 'school_history', 'school_class.school_id = school_history.id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_history.school_id = :school_id AND school_year.id = :school_year_id', [':school_id' => $params->data->schoolId, 'school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schoolClasses;
        } else {
            return null;
        }
    }

    public function actionGetSchools() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId) {
            $schools = School::find()
                ->join('JOIN', 'school_history', 'school.id = school_history.school_id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_year.id = :school_year_id', ['school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schools;
        } else {
            return null;
        }
    }

    protected function findStudentById($id)
    {
        $student = SchoolStudent::find()->where(['id'=>$id])->one();
            return $student;
    }    

}