<?php

namespace app\modules\worker\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\worker\controllers\DefaultController;
use app\modules\worker\models\UploadMarks;
use app\modules\worker\models\SchoolYear;
use app\modules\worker\models\School;
use app\modules\worker\models\SchoolClass;
use app\modules\worker\models\SchoolStudent;
use app\modules\worker\models\Subject;
use app\modules\worker\models\StudentMark;
use yii\web\UploadedFile;

class MarksController extends DefaultController {

    public function actionIndex() {
    	$model = new UploadMarks();
    	$model->schoolYears = SchoolYear::find()->all();
    	$model->subjects = Subject::find()->all();

    	if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())) {
    		$model->uploadFile = UploadedFile::getInstance($model, 'uploadFile');
    		if (!$model->validate()) {
    			\Yii::$app->getSession()->setFlash('error', 'Заполните форму полностью');
    		} else {
    			$file = file_get_contents($model->uploadFile->tempName);
    			if ($file) {
    				$marks = json_decode($file, true);
    				if (!$marks) {
    					\Yii::$app->getSession()->setFlash('error', 'Ошибка в файле с оценками');
    				} else {
    					//if file is correct
    					for ($i = 0, $maxi = count($marks); $i < $maxi; $i++) {
    						//get school student by order in subject
    						//getSchoolStudentByOrder($schoolClassId, $subjectId, $orderNumber)
    						$student = SchoolStudent::getSchoolStudentByOrder($model->schoolClassId, $model->subjectId, $marks[$i]['Order']);
    						//if such student exists
    						if ($student) {
    							//save or update marks
    							for ($j = 0, $maxj = count($marks[$i]['Marks']); $j < $maxj; $j++) { 
    								$studentMark = StudentMark::find()
    									->where('student_mark.mark_date = :markDate AND student_mark.student_id = :student_id AND student_mark.subject_id = :subject_id', ['markDate' => date_format(date_create($marks[$i]['Marks'][$j]['Date']), "Y-m-d"), 'student_id' => $student->id, 'subject_id' => $model->subjectId])
    									->one();
    								if ($studentMark) {
    									$studentMark->mark = implode(',', $marks[$i]['Marks'][$j]['marks']);
    									$studentMark->update();
    								} else {
	    								$studentMark = new StudentMark();
	    								$studentMark->subject_id = $model->subjectId;
	    								$studentMark->student_id = $student->id;
	    								$studentMark->mark_date = date_format(date_create($marks[$i]['Marks'][$j]['Date']), "Y-m-d");
	    								$studentMark->mark = implode(',', $marks[$i]['Marks'][$j]['marks']);
	    								$studentMark->save();
    								}
        						}
    						}
    					}
    					\Yii::$app->getSession()->setFlash('success', 'Оценки загружены');
    				}
    			} else {
    				\Yii::$app->getSession()->setFlash('error', 'Файл с оценками пуст');
    			}
    		}

    		return $this->render('index', [
        		'model' => $model
        	]);
    	} else {
    		return $this->render('index', [
        		'model' => $model
        	]);
    	}
    }

    public function actionGetSchools() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId) {
            $schools = School::find()
                ->join('JOIN', 'school_history', 'school.id = school_history.school_id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_year.id = :school_year_id', ['school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schools;
        } else {
            return null;
        }
    }

    public function actionGetClasses() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId && $params->data->schoolId) {
            $schoolClasses = SchoolClass::find()
                ->join('JOIN', 'school_history', 'school_class.school_id = school_history.id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_history.school_id = :school_id AND school_year.id = :school_year_id', [':school_id' => $params->data->schoolId, 'school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schoolClasses;
        } else {
            return null;
        }
    }
}
