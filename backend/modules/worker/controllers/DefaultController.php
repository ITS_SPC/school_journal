<?php

namespace app\modules\worker\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\worker\models\WorkerProfile;
use app\modules\worker\models\User;
use common\models\ContactForm;
use common\models\AuthAssignment;
use yii\web\NotFoundHttpException;

class DefaultController extends Controller {
	public function behaviors() {
		return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                    	'allow' => true,
                    	'roles' => ['worker'],
                    ],
                ],
            ],
        ];
	}
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function actionIndex() {

        $profile_model = $this->findModel(\Yii::$app->user->identity->id);
        $profile_model->user = User::find()
            ->where(['id' => \Yii::$app->user->identity->id])
            ->one();

        return $this->render('index', [
            'profile_model' => $profile_model,
        ]);
    }
    public function actionContact()
    {   
        $role_name=AuthAssignment::find()->where(['user_id'=>Yii::$app->user->identity->id])->one()->item_name;

        $model = new ContactForm();

        $model->role_name=$role_name;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Спасибо за обращение к нам. Мы ответим как можно быстрее.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке сообщения.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the ParentProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParentProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WorkerProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Нет данных по профилю. Обратитесь к администратору.');
        }
    }
}
