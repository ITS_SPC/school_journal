<?php

namespace app\modules\parent\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\parent\models\StudentScore;
use app\modules\parent\models\StudentScoreFilterResult;

/**
 * ScoreController implements actions for StudentScore model.
 */
class ScoreController extends DefaultController {

    /**
     * Parent profile data.
     * @return mixed
     */
    public function actionIndex() {
        $model = new StudentScore();
        $scoreFilterResultModel = new StudentScoreFilterResult();

        /*go on if filter loaded and valid*/
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            //get student score
            $model->getScore(Yii::$app->user->identity->id);
            if (!count($model->score->student_marks)) {
                \Yii::$app->getSession()->setFlash('warning', 'Нет данных по успеваемости');
            }
            return $this->render('index', [
                'model' => $model
            ]);
        } else {
            return $this->render('index', [
                'model' => $model
            ]);
        }
    }
}
