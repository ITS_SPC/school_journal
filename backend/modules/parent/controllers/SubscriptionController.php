<?php

namespace app\modules\parent\controllers;

use Yii;
use app\modules\parent\models\ParentSubscription;
use app\modules\parent\models\ParentProfile;
use app\modules\parent\models\SchoolYear;
use app\modules\parent\models\PrepareSubscription;
use app\modules\parent\models\SubscriptionRate;
use app\modules\parent\models\SubscriptionForm;
use app\modules\parent\models\ParentStudent;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * SubscriptionController implements the CRUD actions for parent subscription model.
 */
class SubscriptionController extends DefaultController {

    public function beforeAction($action) {
        $this->enableCsrfValidation = (($action->id !== "success") && ($action->id !== "fail")); 
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'result' => [
                'class' => '\robokassa\ResultAction',
                'callback' => [$this, 'resultCallback'],
            ],
            'success' => [
                'class' => '\robokassa\SuccessAction',
                'callback' => [$this, 'successCallback'],
            ],
            'fail' => [
                'class' => '\robokassa\FailAction',
                'callback' => [$this, 'failCallback'],
            ],
        ];
    }

    /**
     * Parent subscription data.
     * @return mixed
     */
    public function actionIndex() {

        $dataProvider = new ActiveDataProvider([
            'query' => ParentSubscription::find()
                ->with(['parentPhone'])
                ->join('JOIN', 'parent_student', 'parent_student.id = parent_subscription.parent_student_id')
                ->where(['parent_student.parent_id' => Yii::$app->user->identity->id])
                ->orderBy([
                    'parent_subscription.subscription_start' => SORT_ASC,
                ]),
        ]);
        return $this->render('index', [
          'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Prepares a new parent subscription model.
     * 
     * @return mixed
     */
    public function actionPrepareSubscription() {
      /*$start    = (new \DateTime('2015-09-09'))->modify('first day of this month');
      $end      = (new \DateTime('2016-05-31'))->modify('first day of this month');
      $interval = \DateInterval::createFromDateString('1 month');
      $period   = new \DatePeriod($start, $interval, $end);

      foreach ($period as $dt) {
          echo $dt->format("Y-m") . "<br>\n";
      }*/
      $parent = ParentProfile::findOne(Yii::$app->user->identity->id);
      $subscription_form = new SubscriptionForm();
      $subscription_form->phones = $parent->parentPhones;
      $subscription_form->students = $parent->students;
      $subscription_form->school_years = SchoolYear::find()->all();

      if ($subscription_form->load(Yii::$app->request->post())) {
      		//check if student in school year
      		$month_rate = SubscriptionRate::getRateByStudentYear($subscription_form->student_id, $subscription_form->school_year_id);
      		if ($month_rate) {

      			//check saubscription range
      			$subscription_start_date = date_format(date_create($subscription_form->subscription_start."-01"), "Y-m-d");

      			$subscription_end_date = date_format(date_create($subscription_form->subscription_end."-01"), "Y-m-d");
      			$subscription_end_date = date("Y-m-t", strtotime($subscription_end_date));
      			if ($subscription_start_date < $subscription_end_date) {

      				//check range in school year
      				$school_year = SchoolYear::findModel($subscription_form->school_year_id);
      				$school_year_start_date = date_format(date_create($school_year->start_year."-09-01"), "Y-m-d");
      				$school_year_end_date = date_format(date_create($school_year->end_year."-05-31"),"Y-m-d");
      				if (($school_year_start_date <= $subscription_start_date) && ($school_year_end_date >= $subscription_end_date)) {

      					//check subscription range intercepts
      					$intercept_subscriptions = ParentSubscription::getSubscriptionDateIntersection($subscription_form->student_id, $parent->id, $subscription_start_date, $subscription_end_date, $subscription_form->phone_id);
      					if (!count($intercept_subscriptions)) {

      						//if all is good -> calculate subscription price
      						$interval = date_diff(date_create($subscription_start_date), date_create($subscription_end_date));
      						$subscription_form->total_rate = $this->diffInMonths(date_create($subscription_start_date), date_create($subscription_end_date)) * $month_rate->value;
							     //echo ($interval->m + ($interval->y * 12) + 1) * $month_rate->value;
      						//print_r($month_rate->value);
      						//\Yii::$app->getSession()->setFlash('success', 'All good');
      						return $this->render('confirm-subscription', [
              					'subscription_form' => $subscription_form,
          					]);

      					} else {
      							$subscription_form->total_rate = null;
      							\Yii::$app->getSession()->setFlash('error', 'Пересечение с ранее созданными подписками');
      							return $this->render('prepare-subscription', [
              						'subscription_form' => $subscription_form,
          						]);

      					}
      					/*\Yii::$app->getSession()->setFlash('success', 'All good');
      					return $this->render('create', [
              				'subscription_form' => $subscription_form,
          				]);*/

      				} else {
      					$subscription_form->total_rate = null;
      					\Yii::$app->getSession()->setFlash('error', 'Диапазон дат подписки не попадает в учебный год');
      					return $this->render('prepare-subscription', [
              				'subscription_form' => $subscription_form,
          				]);

      				}
      			} else {
      				$subscription_form->total_rate = null;
      				\Yii::$app->getSession()->setFlash('error', 'Неверно задан диапазон дат подписки');
      				return $this->render('prepare-subscription', [
              			'subscription_form' => $subscription_form,
          			]);

      			}

      		} else {
      			$subscription_form->total_rate = null;
      			\Yii::$app->getSession()->setFlash('error', 'Учащегося нет в этом учебном году');
      			return $this->render('prepare-subscription', [
              		'subscription_form' => $subscription_form,
          		]);
      		}
      } else {
          return $this->render('prepare-subscription', [
              'subscription_form' => $subscription_form,
          ]);
      }
    }

    /**
    * Pay thrue robokassa
    */
    public function actionPayment() {
      $subscription_form = new SubscriptionForm();
      $subscription = new ParentSubscription();
      $parent = ParentProfile::findOne(Yii::$app->user->identity->id);
      $subscription_form->phones = $parent->parentPhones;
      $subscription_form->students = $parent->students;
      $subscription_form->school_years = SchoolYear::find()->all();
      if ($subscription_form->load(Yii::$app->request->post())) {

        if ($subscription_form->load(Yii::$app->request->post())) {
          //check if student in school year
          $month_rate = SubscriptionRate::getRateByStudentYear($subscription_form->student_id, $subscription_form->school_year_id);
          if ($month_rate) {

            //check saubscription range
            $subscription_start_date = date_format(date_create($subscription_form->subscription_start."-01"), "Y-m-d");

            $subscription_end_date = date_format(date_create($subscription_form->subscription_end."-01"), "Y-m-d");
            $subscription_end_date = date("Y-m-t", strtotime($subscription_end_date));
            if ($subscription_start_date < $subscription_end_date) {

              //check range in school year
              $school_year = SchoolYear::findModel($subscription_form->school_year_id);
              $school_year_start_date = date_format(date_create($school_year->start_year."-09-01"), "Y-m-d");
              $school_year_end_date = date_format(date_create($school_year->end_year."-05-31"),"Y-m-d");
              if (($school_year_start_date <= $subscription_start_date) && ($school_year_end_date >= $subscription_end_date)) {

                //check subscription range intercepts
                $intercept_subscriptions = ParentSubscription::getSubscriptionDateIntersection($subscription_form->student_id, $parent->id, $subscription_start_date, $subscription_end_date, $subscription_form->phone_id);
                if (!count($intercept_subscriptions)) {

                  //if all is good -> calculate subscription price
                  //get parent student relation
                  $parent_student = ParentStudent::find()
                    ->where(['student_id' => $subscription_form->student_id, 'parent_id' => \Yii::$app->user->identity->id])
                    ->one();
                  $subscription_form->total_rate = $this->diffInMonths(date_create($subscription_start_date), date_create($subscription_end_date)) * $month_rate->value;
                  $subscription->parent_student_id = $parent_student->id;
                  $subscription->parent_phone_id = $subscription_form->phone_id;
                  $subscription->amount = $subscription_form->total_rate;
                  $subscription->subscription_start = date_format(date_create($subscription_form->subscription_start."-01"), "Y-m-d");
                  $subscription->subscription_end = date("Y-m-t", strtotime($subscription_form->subscription_end));
                  if ($subscription->save()) {
                    $merchant = Yii::$app->get('robokassa');
                    return $merchant->payment($subscription_form->total_rate, $subscription->id, 'Подписка с '.$subscription->subscription_start.' до '.$subscription->subscription_end, null, Yii::$app->user->identity->email);
                  } else {
                    $subscription_form->total_rate = null;
                    \Yii::$app->getSession()->setFlash('error', 'Ошибка при сохранении подписки');
                    return $this->render('prepare-subscription', [
                          'subscription_form' => $subscription_form,
                      ]);
                  }

                } else {
                    $subscription_form->total_rate = null;
                    \Yii::$app->getSession()->setFlash('error', 'Пересечение с ранее созданными подписками');
                    return $this->render('prepare-subscription', [
                          'subscription_form' => $subscription_form,
                      ]);

                }
                /*\Yii::$app->getSession()->setFlash('success', 'All good');
                return $this->render('create', [
                      'subscription_form' => $subscription_form,
                  ]);*/

              } else {
                $subscription_form->total_rate = null;
                \Yii::$app->getSession()->setFlash('error', 'Диапазон дат подписки не попадает в учебный год');
                return $this->render('prepare-subscription', [
                      'subscription_form' => $subscription_form,
                  ]);

              }
            } else {
              $subscription_form->total_rate = null;
              \Yii::$app->getSession()->setFlash('error', 'Неверно задан диапазон дат подписки');
              return $this->render('prepare-subscription', [
                    'subscription_form' => $subscription_form,
                ]);

            }

          } else {
            $subscription_form->total_rate = null;
            \Yii::$app->getSession()->setFlash('error', 'Учащегося нет в этом учебном году');
            return $this->render('prepare-subscription', [
                  'subscription_form' => $subscription_form,
              ]);
          }
      } else {
          return $this->render('prepare-subscription', [
              'subscription_form' => $subscription_form,
          ]);
      }
      }
      
    }

    public function successCallback($merchant, $nInvId, $nOutSum, $shp) {
      \Yii::$app->getSession()->setFlash('success', 'Подписка оформлена');
      $this->findModel($nInvId)->updateAttributes(['payment_status' => 1]);
      return $this->redirect(['index']);
    }

    public function resultCallback($merchant, $nInvId, $nOutSum, $shp) {
      $this->findModel($nInvId)->updateAttributes(['payment_status' => 1]);
      return $this->redirect(['index']);
    }

    public function failCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        \Yii::$app->getSession()->setFlash('error', 'Произошла ошибка при оплате. Свяжитесь с администратором.');
        return $this->redirect(['index']);
    }

    public function diffInMonths(\DateTime $date1, \DateTime $date2) {
      $diff =  $date1->diff($date2);

      $months = $diff->y * 12 + $diff->m + $diff->d / 30;

      return (int) round($months);
    }

    public function actionRate() {
        $params = json_decode(file_get_contents('php://input'));
        $rate_value = SubscriptionRate::getRateByStudentYear($params->data->studentId, $params->data->schoolYearId);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $rate_value;
    }

    /**
     * Updates an existing parent subscription model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Deletes an existing parent subscription model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the parent subscription model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParentProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParentSubscription::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
