<?php

namespace app\modules\parent\controllers;

use Yii;
use app\modules\parent\models\ParentPhone;
use app\modules\parent\models\ParentSubscription;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PhoneController implements the CRUD actions for phone model.
 */
class PhoneController extends DefaultController {

    /**
     * Parent profile data.
     * @return mixed
     */
    public function actionIndex() {
        //get current user profile
        $this->goHome();
    }

    /**
     * Updates an existing ParentPhone model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['profile/']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ParentPhone model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        if (!$model->delete()) {
           Yii::$app->session->setFlash('error', 'Невозможно удалить телефон, на него оформлена подписка.'); 
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the ParentPhone model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParentPhone the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParentPhone::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
