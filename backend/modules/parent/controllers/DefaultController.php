<?php

namespace app\modules\parent\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\modules\parent\models\ParentProfile;
use app\modules\parent\models\SchoolStudent;
use app\modules\parent\models\SchoolClass;
use app\modules\parent\models\Student;
use app\modules\parent\models\ContactForm;

class DefaultController extends Controller {
	public function behaviors() {
		return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                    	'allow' => true,
                    	'roles' => ['parent'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
	}

    public function actionIndex() {
        //get school_students of parent
        $dataProvider = new ActiveDataProvider([
            'query' => Student::find()
                ->leftJoin('parent_student', '`parent_student`.`student_id` = `student`.`id`')
                ->where(['parent_student.parent_id' => Yii::$app->user->identity->id])
                ->orderBy([
                    'student.surname' => SORT_ASC,
                    'student.name' => SORT_ASC
                ]),
        ]);
        return $this->render('view', [
            'dataProvider' => $dataProvider
        ]);
    }
    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Спасибо, что обратились к нам. Мы ответим Вам как можно скорее.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке сообщения.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
    protected function findModel($id)
    {
        if (($model = ParentProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Не задан профиль пользователя');
        }
    }
}
