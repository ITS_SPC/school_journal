<?php

namespace app\modules\parent\controllers;

use Yii;
use app\modules\parent\models\ParentProfile;
use app\modules\parent\models\ParentPhone;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProfileController implements the CRUD actions for ParentProfile model.
 */
class ProfileController extends DefaultController {

    /**
     * Parent profile data.
     * @return mixed
     */
    public function actionIndex() {
        //get current user profile
        $profile_model = $this->findModel(Yii::$app->user->identity->id);
        $phone_model = new ParentPhone();
        $dataProvider = new ActiveDataProvider([
            'query' => ParentPhone::find()
                ->where(['parent_id' => Yii::$app->user->identity->id])
        ]);

        if ($phone_model->load(Yii::$app->request->post())) {
            $phone_model->parent_id = Yii::$app->user->identity->id;
            if ($phone_model->save()) {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('index', [
            'profile_model' => $profile_model,
            'phone_model' => $phone_model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the ParentProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParentProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ParentProfile::findOne($id)) !== null) {
            return $model;
        } /*else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }*/
    }
}
