<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */

$this->title = 'Изменить номер телефона: ' . ' ' . $model->phone_number;
$this->params['breadcrumbs'][] = ['label' => 'Профиль', 'url' => ['profile/']];
$this->params['breadcrumbs'][] = ['label' => 'Изменение телефона'];
?>
<div class="parent-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
