<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
 
/* @var $this yii\web\View */
/* @var $model app\models\ParentPhone */
/* @var $form yii\widgets\ActiveForm */
?>
 
<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
        $("#parentphone-phone_number").mask("+7(999)999-9999", { completed: function () { $(".hidden").slideDown("slow"); } });
    });'
);
?>
 
<div class="phones-form">
 
    <?php $form = ActiveForm::begin(); ?>
 
        <?= $form->field($model, 'phone_number')->textInput(['maxlength' => 200]) ?>
 
 
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
 
    <?php ActiveForm::end(); ?>
    
</div>