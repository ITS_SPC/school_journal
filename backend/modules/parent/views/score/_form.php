<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\ScoreTableWidget;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parent-profile-form">

    <div class="panel panel-primary">
        <div class="panel-heading">Поиск учащегося</div>
        <div class="panel-body">

            <?php $form = ActiveForm::begin([
                'id' => 'student-score-filter',
                'options' => ['class' => ''],
            ]); ?>

                <div class="row">
                    <div class="col-md-4">

                        <?= $form->field($model, 'school_student_id')->dropdownList(ArrayHelper::map($model->getParentStudents(Yii::$app->user->identity->id), 'id',
                            function($model, $defaultValue) {
                                return $model->surname.' '.$model->name;
                            }), ['prompt' => '---Учащиеся---']) ?>

                    </div>
                    <div class="col-md-4">

                        <?= $form->field($model, 'school_year_id')->dropdownList(ArrayHelper::map($model->getSchoolYears(),'id',
                        function($model, $defaultValue) {
                            return $model->start_year.'-'.$model->end_year;
                        }), ['prompt' => '---Учебный год---']) ?>

                    </div>
                    <div class="col-md-4">

                        <?= $form->field($model, 'school_subject_id')->dropdownList(ArrayHelper::map($model->getSubjects(),'id',
                        function($model, $defaultValue) {
                            return $model->name;
                        }), ['prompt' => '---Все предметы---']) ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= Html::submitButton('Показать', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">Успеваемость учащегося</div>
        <div class="panel-body">

            <?= 
                ScoreTableWidget::widget([
                    'model' => $model->score,
                ]);
            ?>

        </div>
    </div>
</div>
