<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мой профиль';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('view', [
    	'profile_model' => $profile_model,
    ]) ?>

    <h1>Мобильные телефоны</h1>

    <?= $this->render('_phone_form', [
        'phone_model' => $phone_model,
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
            'columns' => [
              	['class' => 'yii\grid\SerialColumn'],
                'phone_number',
                [
                	'class' => 'yii\grid\ActionColumn',
                	'controller' => 'phone',
                	'template' => '{update}   {delete}',
                ],
            ],
        ]); ?>

</div>
