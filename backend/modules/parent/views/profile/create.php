<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */

$this->title = 'Create Parent Profile';
$this->params['breadcrumbs'][] = ['label' => 'Parent Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
