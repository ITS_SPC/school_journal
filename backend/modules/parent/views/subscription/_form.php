<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div ng-app='subscribeApp' class="parent-profile-form">
    
    <?php $form = ActiveForm::begin(['action' => 'prepare-subscription','options' => ['method' => 'post']]); ?>

    <div class="row">
        <div class="col-md-4">
            
            <?= $form->field($subscription_form, 'student_id')->dropdownList(ArrayHelper::map($subscription_form->students, 'id',
                            function($model, $defaultValue) {
                                return $model->surname.' '.$model->name;
                            }), ['prompt' => '---Учащийся---', 'ng-model' => 'subscriptionForm.studentId']) ?>

        </div>
        <div class="col-md-4">
            
            <?= $form->field($subscription_form, 'phone_id')->dropdownList(ArrayHelper::map($subscription_form->phones, 'id',
                            function($model, $defaultValue) {
                                return $model->phone_number;
                            }), ['prompt' => '---Моб. телефон---', 'ng-model' => 'subscriptionForm.phoneId']) ?>

        </div>
        <div class="col-md-4">
            
            <?= $form->field($subscription_form, 'school_year_id')->dropdownList(ArrayHelper::map($subscription_form->school_years, 'id',
                            function($model, $defaultValue) {
                                return $model->start_year." / ".$model->end_year;
                            }), ['prompt' => '---Учебный год---', 'ng-model' => 'subscriptionForm.schoolYearId']) ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            
            <?= $form->field($subscription_form, 'subscription_start')->widget(DatePicker::className(), [
                            'template' => '{addon}{input}',
                            'language' => 'ru',
                            'clientOptions' => [
                                'startView' => 1,
                                'minViewMode' => 1,
                                'maxViewMode' => 1,
                                'format' => 'yyyy-mm',
                                'multidateSeparator' => ' ; ',
                                'todayBtn' => "linked",
                                'clearBtn' => true,
                                'autoclose' => true,
                            ],
                            'options' => ['ng-model' => 'subscriptionForm.subscriptionStart'],
                        ]);
                    ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($subscription_form, 'subscription_end')->widget(DatePicker::className(), [
                            'template' => '{addon}{input}',
                            'language' => 'ru',
                            'clientOptions' => [
                                'startView' => 1,
                                'minViewMode' => 1,
                                'maxViewMode' => 1,
                                'format' => 'yyyy-mm',
                                'multidateSeparator' => ' ; ',
                                'todayBtn' => "linked",
                                'clearBtn' => true,
                                'autoclose' => true,
                            ],
                            'options' => ['ng-model' => 'subscriptionForm.subscriptionEnd']
                        ]);
                    ?>

        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            
            <?= ($subscription_form->total_rate) ? ($form->field($subscription_form, 'total_rate')) : ("") ?>

        </div>
    </div>
    <div class="row text-right">
        <div class="col-md-12">

            <?= Html::submitButton('Подсчитать', ['class' => 'btn btn-primary']) ?>

        </div>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
