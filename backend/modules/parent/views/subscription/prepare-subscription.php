<?php

use yii\helpers\Html;
//use backend\assets\SubscriptionAsset;

/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */
//SubscriptionAsset::register($this);
$this->title = 'Оформление подписки';
$this->params['breadcrumbs'][] = ['label' => 'Подписки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-profile-create">

	<div class="panel panel-default">
  		<div class="panel-heading">
  			<h1>

  				<?= Html::encode($this->title) ?>

  			</h1>
  		</div>
  		<div class="panel-body">
  			<?= $this->render('_form', [
        		'subscription_form' => $subscription_form
    		]) ?>
  		</div>
	</div>
</div>
