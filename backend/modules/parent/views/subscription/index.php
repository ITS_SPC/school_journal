<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История подписок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
    	<?php /*Html::a('Создать заявку на подписку', ['subscription/create'], [ 
            'class' => 'btn btn-success', 
        ]) */?>
    </p>

    <?= $this->render('view', [
        'dataProvider' => $dataProvider
    ]) ?>

</div>
