<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */
?>
<div class="parent-profile-view">
    <div class="row">
        <div class="col-md-12">
            <?= \yii\helpers\Html::a( 'Оформить подписку', ['prepare-subscription'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Учащийся',
                'value' => function ($data) {
                    return $data->parentStudent->student->name;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Начало подписки',
                'value' => function ($data) {
                    return date_format(date_create($data->subscription_start), "Y-m-d");
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Окончание подписки',
                'value' => function ($data) {
                    return $data->subscription_end;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Телефон',
                'value' => function ($data) {
                    return $data->parentPhone->phone_number;
                },
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'label' => 'Статус оплаты',
                'attribute' => 'payment_status',
                'value' => function ($data) {
                    switch ($data->payment_status) {
                        case true:
                            return "Оплачено";
                            break;
                        
                        default:
                            return "Не оплачено";
                            break;
                    }
                },
            ]

            /*[
                'class' => 'yii\grid\ActionColumn',
                'template' => '{disable-subscription}',
                'buttons' => [
                    'disable-subscription' => function ($url, $model) {
                        return Html::a(Html::checkbox('payment_status', true), $url, 
                        [
                            'title' => Yii::t('app', 'Change today\'s lists'),
                        ]);;
                    }
                ],
            ],*/
        ],
    ]); ?>
    </div>
    </div>

</div>
