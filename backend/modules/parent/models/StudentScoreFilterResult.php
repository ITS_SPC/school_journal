<?php

namespace app\modules\parent\models;

use Yii;
use yii\base\Model;
use app\modules\parent\models\SchoolYear;
use app\modules\parent\models\SchoolStudent;
use app\modules\parent\models\SchoolSubject;

class StudentScoreFilterResult extends Model {

	public $school_student_id;
	public $school_year_id;
	public $school_subject_id;
	public $parent_students;
	public $school_years;
	public $calendar;
	public $student_marks;
	public $student_subscription;

	public function attributeLabels() {
        return [
            'school_student_id' => 'Учащийся',
            'school_year_id' => 'Учебный год',
            'school_subject_id' => 'Предмет',
        ];
    }

    public function rules() {
    	return [
        	[['school_student_id', 'school_year_id'], 'required'],
        	[['school_class_id', 'school_year_id', 'school_subject_id'], 'integer'],
    	];
	}

	public function getSchoolYears() {
		return SchoolYear::find()->all();
	}

	public function getParentStudents($parent_id) {
		return SchoolStudent::find()
                ->leftJoin('parent_student', '`parent_student`.`student_id` = `school_student`.`id`')
                ->where(['parent_student.parent_id' => $parent_id])->all();
	}

	public function getSubjects() {
		return SchoolSubject::find()
			->all();
	}
}