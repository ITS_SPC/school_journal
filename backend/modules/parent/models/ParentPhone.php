<?php

namespace app\modules\parent\models;

use Yii;
use app\modules\parent\models\ParentSubscription;

/**
 * This is the model class for table "parent_phone".
 *
 * @property integer $id
 * @property string $phone_number
 * @property integer $parent_id
 *
 * @property Parent $parent
 * @property ParentSubscription[] $parentSubscriptions
 */
class ParentPhone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parent_phone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone_number', 'parent_id'], 'required'],
            [['phone_number'], 'unique'],
            [['parent_id'], 'integer'],
            [['phone_number'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone_number' => 'Номер телефона',
            'parent_id' => 'Parent ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Parent::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentSubscriptions()
    {
        return $this->hasMany(ParentSubscription::className(), ['parent_phone_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if (ParentSubscription::find()->where(['parent_phone_id' => $this->id])->exists()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

}
