<?php

namespace app\modules\parent\models;

use Yii;
use yii\base\Model;


/**
 * This is the model class for student preparing subscription entitie.
 *
 * @property integer $student_id
 * @property integer $school_year_id
 *
 */
class PrepareSubscription extends Model {

	public $student_id;
	public $school_year_id;

	public function attributeLabels() {
        return [
            'student_id' => 'Учащийся',
            'school_year_id' => 'Учебный год'
        ];
    }

    public function rules() {
    	return [
        	[['student_id', 'school_year_id'], 'required'],
        	[['student_id', 'school_year_id'], 'integer'],
    	];
	}
}