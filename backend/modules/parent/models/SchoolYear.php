<?php

namespace app\modules\parent\models;

use Yii;

/**
 * This is the model class for table "school_year".
 *
 * @property integer $id
 * @property string $start_year
 * @property string $end_year
 *
 * @property School[] $schools
 */
class SchoolYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_year', 'end_year'], 'required'],
            [['start_year', 'end_year'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'start_year' => 'Start Year',
            'end_year' => 'End Year',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchools()
    {
        return $this->hasMany(School::className(), ['school_year_id' => 'id']);
    }

    public static function findModel($id) {
        return SchoolYear::findOne($id);
    }
}
