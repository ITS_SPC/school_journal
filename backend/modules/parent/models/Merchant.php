<?php

namespace app\modules\parent\models;

use Yii;
use yii\base\Object;

class Merchant extends Object {

	public $sMerchantLogin;

    public $sMerchantTestPass1;
    public $sMerchantTestPass2;

    public $baseUrl = 'http://auth.robokassa.ru/Merchant/Index.aspx';

    /**
    * $nOutSum - стоимость заказа
    * $nInvId - номер счета (id подписки)
    * $sInvDesc - описание покупки
    * $sIncCurrLabel - способ оплаты
    * $sEmail - e-mail подписчика
    * $sCulture - язык общения с клиентом
    * $shp - дополнительные параметры
    */
    public function payment($nOutSum, $nInvId, $sInvDesc = null, $sIncCurrLabel=null, $sEmail = null, $sCulture = null, $shp = [], $isTest = 0) {
        $url = $this->baseUrl;

        $signature = "{$this->sMerchantLogin}:{$nOutSum}:{$nInvId}:{$this->sMerchantTestPass1}";
        if (!empty($shp)) {
            $signature .= ':' . $this->implodeShp($shp);
        }
        $sSignatureValue = md5($signature);

        $url .= '?' . http_build_query([
            'MerchantLogin' => $this->sMerchantLogin,
            'OutSum' => $nOutSum,
            'InvoiceId' => $nInvId,
            'Description' => $sInvDesc,
            'SignatureValue' => $sSignatureValue,
            'IncCurrLabel' => $sIncCurrLabel,
            //'Email' => $sEmail,
            'Culture' => $sCulture,
            'IsTest' => $isTest
        ]);

        if (!empty($shp) && ($query = http_build_query($shp)) !== '') {
            $url .= '&' . $query;
        }

        Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());

        return Yii::$app->response->redirect($url);
    }
}