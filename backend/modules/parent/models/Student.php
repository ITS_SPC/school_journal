<?php

namespace app\modules\parent\models;

use Yii;

/**
 * This is the model class for table "student".
 *
 * @property integer $id
 * @property string $surname
 * @property string $name
 * @property string $lastname
 *
 * @property ParentStudent[] $parentStudents
 * @property Parent[] $parents
 * @property SchoolStudent[] $schoolStudents
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['surname', 'name', 'lastname'], 'required'],
            [['surname', 'name', 'lastname'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'lastname' => 'Отчество',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentStudents()
    {
        return $this->hasMany(ParentStudent::className(), ['student_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(Parent::className(), ['id' => 'parent_id'])->viaTable('parent_student', ['student_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolStudents()
    {
        return $this->hasMany(SchoolStudent::className(), ['personal_number' => 'id']);
    }

    public static function findModel($id) {
        return Student::findOne($id);
    }
}
