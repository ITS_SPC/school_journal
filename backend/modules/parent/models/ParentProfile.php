<?php

namespace app\modules\parent\models;

use Yii;
use app\modules\parent\models\Student;

/**
 * This is the model class for table "parent".
 *
 * @property integer $id
 * @property string $surname
 * @property string $name
 * @property string $lastname
 * @property double $score
 * @property string $address
 *
 * @property User $id0
 * @property ParentPhone[] $parentPhones
 * @property ParentStudent[] $parentStudents
 * @property Student[] $students
 */
class ParentProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['surname', 'name', 'lastname', 'score', 'address'], 'required'],
            [['score'], 'number'],
            [['surname', 'name', 'lastname', 'address'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'lastname' => 'Отчество',
            'score' => 'Score',
            'address' => 'Адрес',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentPhones()
    {
        return $this->hasMany(ParentPhone::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentStudents()
    {
        return $this->hasMany(ParentStudent::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['id' => 'student_id'])->viaTable('parent_student', ['parent_id' => 'id']);
    }
}
