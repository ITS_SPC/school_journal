<?php
namespace app\modules\parent\models;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SubscriptionForm extends Model
{
    public $student_id;
    public $phone_id;
    public $subscription_start;
    public $subscription_end;
    public $school_year_id;
    public $total_rate;
    public $ready_to_subscribe;

    public $phones;
    public $students;
    public $school_years;


    public function attributeLabels() {
        return [
            'student_id' => 'Учащийся',
            'phone_id' => 'Телефон подписки',
            'subscription_start' => 'Начало подписки',
            'subscription_end' => 'Конец подписки',
            'total_rate' => 'Сумма к оплате, руб.',
            'school_year_id' => 'Учебный год'
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['student_id', 'phone_id', 'subscription_dates', 'total_rate', 'school_year_id', 'subscription_start', 'subscription_end'], 'required'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    /*public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }*/
}
