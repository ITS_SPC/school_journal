<?php

namespace app\modules\parent\models;

use Yii;

/**
 * This is the model class for table "subscription_rate".
 *
 * @property integer $id
 * @property double $value
 *
 * @property SchoolHistory[] $schoolHistories
 */
class SubscriptionRate extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['value'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Стоимость месяца',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolHistories()
    {
        return $this->hasMany(SchoolHistory::className(), ['subscription_rate_id' => 'id']);
    }

    public static function getRateByStudentYear($student_id, $school_year_id) {
        $model = SubscriptionRate::find()
            ->join('JOIN', 'school_history', 'subscription_rate.id = school_history.subscription_rate_id')
            ->join('JOIN', 'school_year', 'school_history.school_year_id = school_year.id')
            ->join('JOIN', 'school_class', 'school_history.id = school_class.school_id')
            ->join('JOIN', 'school_student', 'school_class.id = school_student.school_class_id')
            ->where('school_student.personal_number = :student_id AND school_year.id = :school_year_id', [':student_id' => $student_id, 'school_year_id' => $school_year_id])
            ->one();
        return $model;
    }
}
