<?php

namespace app\modules\parent\models;

use Yii;

/**
 * This is the model class for table "parent_student".
 *
 * @property integer $parent_id
 * @property integer $student_id
 * @property integer $id
 *
 * @property Parent $parent
 * @property Student $student
 * @property ParentSubscription[] $parentSubscriptions
 */
class ParentStudent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parent_student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'student_id'], 'required'],
            [['parent_id', 'student_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parent_id' => 'Parent ID',
            'student_id' => 'Student ID',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Parent::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentSubscriptions()
    {
        return $this->hasMany(ParentSubscription::className(), ['parent_student_id' => 'id']);
    }
}