<?php

namespace app\modules\parent\models;

use Yii;
use yii\base\Model;
use app\modules\parent\models\SchoolYear;
use app\modules\parent\models\Student;
use app\modules\parent\models\SchoolStudent;
use app\modules\parent\models\SchoolSubject;
use app\modules\parent\models\StudentScoreFilterResult;
use app\modules\parent\models\StudentMark;
use app\modules\parent\models\ParentSubscription;


/**
 * This is the model class for student score entitie.
 *
 * @property integer $school_student_id
 * @property integer $school_year_id
 * @property integer $school_subject_id
 *
 * @property StudentScoreFilterResult $score
 */
class StudentScore extends Model {

	public $school_student_id;
	public $school_year_id;
	public $school_subject_id;
	public $score;

	public function attributeLabels() {
        return [
            'school_student_id' => 'Учащийся',
            'school_year_id' => 'Учебный год',
            'school_subject_id' => 'Предмет',
        ];
    }

    public function rules() {
    	return [
        	[['school_student_id', 'school_year_id'], 'required'],
        	[['school_year_id', 'school_subject_id'], 'integer'],
    	];
	}

	public function getSchoolYears() {
		return SchoolYear::find()->all();
	}

	public function getParentStudents($parent_id) {
		return Student::find()
                ->leftJoin('parent_student', '`parent_student`.`student_id` = `student`.`id`')
                ->where(['parent_student.parent_id' => $parent_id])
                ->all();
	}

	public function getSubjects() {
		return SchoolSubject::find()
			->all();
	}

	public function getSchoolYear() {
		return SchoolYear::findOne($this->school_year_id);
	}

	public function getScore($parent_id) {
		$score = new StudentScoreFilterResult();

		$filter_data = array();
		/*check if subject slected*/
		if ($this->school_subject_id) {
			$filter_data[] = "student_mark.subject_id = '".$this->school_subject_id."'";
		}
		/*get student marks in school year*/
		$score->student_marks = StudentMark::find()
    		->join('JOIN', 'school_student', 'school_student.id = student_mark.student_id')
    		->join('JOIN', 'school_class', 'school_student.school_class_id = school_class.id')
    		->join('JOIN', 'school_history', 'school_history.id = school_class.school_id')
    		->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
            /*->join('JOIN', 'parent_student', 'school_student.id = parent_student.student_id')
            ->join('JOIN', 'parent_subscription', 'parent_student.student_id = parent_subscription.student_id')*/
            ->where('school_student.personal_number = :student_id AND school_year.id = :school_year_id', [':student_id' => $this->school_student_id, 'school_year_id' => $this->school_year_id])
            ->andWhere(implode("", $filter_data))
    		//->where(['student_mark.student_id' => $this->school_student_id, 'school_year.id' => $this->school_year_id, 'student_mark.subject_id' => $this->school_subject_id])
    		->orderBy([
           		'student_mark.mark_date' => SORT_ASC,
        	])
    		->all();
    	/*get all subscription of student in school year*/
        $score->student_subscription = ParentSubscription::find()
            ->join('JOIN', 'parent_student', 'parent_student.id = parent_subscription.parent_student_id')
            ->join('JOIN', 'school_student', 'school_student.personal_number = parent_student.student_id')
            ->join('JOIN', 'school_class', 'school_student.school_class_id = school_class.id')
            ->join('JOIN', 'school_history', 'school_history.id = school_class.school_id')
            ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
            ->where('parent_student.parent_id = :parent_id AND parent_student.student_id = :student_id AND school_year.id = :school_year_id AND parent_subscription.payment_status = 1', [':student_id' => $this->school_student_id, 'school_year_id' => $this->school_year_id, 'parent_id' => $parent_id])
            ->all();
        /*prepare score calendar for student*/
        $score->calendar = array();
        foreach ($score->student_marks as $mark) {
        	$score->calendar[] = $mark->mark_date;
        }
        /*remove repeating dates from calendar*/
        $score->calendar = array_unique($score->calendar);
        /*sort calendar dates by asc*/
        sort($score->calendar);
        /*group marks by subject*/
        $sorted_data = array();
        foreach ($score->student_marks as $student_marks_subjects) {
           	$sorted_data[$student_marks_subjects['subject_id']][] = $student_marks_subjects;
        }
        $score->student_marks = $sorted_data;

        $this->score = $score;
	}

	/* takes two dates formatted as YYYY-MM-DD and creates an
    inclusive array of the dates between the from and to dates.
    could test validity of dates here but I'm already doing
    that in the main script*/
	private function createDateRangeArray($strDateFrom, $strDateTo) {
    	$aryRange = array();
    	$iDateFrom = mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    	$iDateTo = mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    	if ($iDateTo >= $iDateFrom) {
        	array_push($aryRange, date('Y-m-d',$iDateFrom)); // first entry
        	while ($iDateFrom < $iDateTo) {
            	$iDateFrom += 86400; // add 24 hours
            	array_push($aryRange,date('Y-m-d',$iDateFrom));
        	}
    	}
    	return $aryRange;
	}
}