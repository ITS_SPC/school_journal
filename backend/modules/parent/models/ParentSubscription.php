<?php

namespace app\modules\parent\models;

use Yii;
use app\modules\parent\models\ParentStudent;

/**
 * This is the model class for table "parent_subscription".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $student_id
 * @property string $subscription_start
 * @property string $subscription_end
 * @property integer $parent_phone_id
 * @property integer $payment_status
 * @property double $amount
 *
 * @property ParentStudent $parent
 * @property ParentStudent $student
 * @property ParentPhone $parentPhone
 */
class ParentSubscription extends \yii\db\ActiveRecord
{
    public $school_year_id;
    public $subscription_rate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parent_subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_student_id', 'subscription_start', 'subscription_end', 'parent_phone_id', 'amount'], 'required'],
            [['parent_student_id', 'parent_phone_id', 'payment_status'], 'integer'],
            [['amount'], 'number'],
            [['subscription_start', 'subscription_end'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_student_id' => 'Учащийся',
            'subscription_start' => 'Начало подписки',
            'subscription_end' => 'Окончание подписки',
            'parent_phone_id' => 'Мобильный телефон',
            'amount' => 'Сумма платежа'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentStudent()
    {
        return $this->hasOne(ParentStudent::className(), ['id' => 'parent_student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentPhone()
    {
        return $this->hasOne(ParentPhone::className(), ['id' => 'parent_phone_id']);
    }

    public static function getSubscriptionDateIntersection($student_id, $parent_id, $subscription_start, $subscription_end, $phone_id) {
        //get student id
        $student = ParentStudent::find()
            ->where('parent_id = :parent_id AND student_id = :student_id', ['parent_id' => $parent_id, 'student_id' => $student_id])
            ->one();
        if ($student) {
            //BeginDate1 <= EndDate2 AND BeginDate2 <= EndDate1
            $subscriptions = ParentSubscription::find()
                ->where('subscription_start <= :subscription_end AND :subscription_start <= subscription_end AND parent_student_id = :parent_student_id AND parent_phone_id = :phone_id', [':subscription_start' => $subscription_start, 'subscription_end' => $subscription_end, 'parent_student_id' => $student->id, 'phone_id' => $phone_id])
                ->all();
            return $subscriptions;
        } else {
            return [];
        }
    }
}
