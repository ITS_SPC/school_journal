<?php

namespace app\modules\parent;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\parent\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
