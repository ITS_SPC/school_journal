<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\Student;
use app\modules\administrator\models\SchoolStudent;
use app\modules\administrator\models\ParentStudent;
use app\modules\administrator\models\ParentSubscription;
use app\modules\administrator\models\StudentMark;
use app\modules\administrator\models\StudentSubject;
use app\modules\administrator\models\StudentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\controllers\DefaultController;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends DefaultController
{
    /*public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Student model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Student();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {	
		//delete item from school_student table by student id
		$this->deleteSchoolStudent($id);
		
		//delete item from parent_student table by student id
		$this->deleteParentStudent($id);
		
		$this->findModel($id)->delete();
		
       return $this->redirect(['index']);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */ 
    protected function findModel($id)
    {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    protected function deleteSchoolStudent($student_id)
	{
		$school_student = (new \yii\db\Query())
				->select(['*'])
				->from('school_student')
				->where(['personal_number' => $student_id])
				->all();
		if(empty($school_student)){
			return;
		}else{
 			$models=SchoolStudent::find()->where(['personal_number'=>$student_id])->all();
			foreach($models as $v){
				$this->deleteStudentMarks($v->id);
				$this->deleteStudentSubject($v->id);
				$v->delete();
			}		
		} 
	}
	/**
	 * Deletes student marks from student_mark table by school_student_id
	 */
	protected function deleteStudentMarks($school_student_id)
	{
	$student_mark = (new \yii\db\Query())
				->select(['*'])
				->from('student_mark')
				->where(['student_id' => $school_student_id])
				->all();
		if(empty($student_mark)){
			return;
		}else{
			$models=StudentMark::find()->where(['student_id'=>$school_student_id])->all();
			foreach($models as $v){
				$v->delete();
			}
		}
	}
	
	/**
	 * Deletes student subjects from student_subject table by school_student_id
	 */
	protected function deleteStudentSubject($school_student_id){
		$student_subject = (new \yii\db\Query())
				->select(['*'])
				->from('student_subject')
				->where(['student_id' => $school_student_id])
				->all();
		if(empty($student_subject)){
			return;
		}else{
			$models=StudentSubject::find()->where(['student_id'=>$school_student_id])->all();
			foreach($models as $v){
				$v->delete();
			}
		}
	}
	
	/**
	 * Deletes parent-student relation by deleting
	 * item from parent_student table by student id
	 */
	protected function deleteParentStudent($student_id)
	{
		$parent_student = (new \yii\db\Query())
				->select(['*'])
				->from('parent_student')
				->where(['student_id' => $student_id])
				->all();
		if(empty($parent_student)){
			return;
		}else{
			$models=ParentStudent::find()->where(['student_id'=>$student_id])->all();
			foreach($models as $v){
				$this->deleteParentSubscription($v->id);
				$v->delete();
			}		
		}											
	}
	/**
	 * Finds the ParentSubscription model where parent_student_id
	 * associated with similar item of parent_student table
	 */
	protected function deleteParentSubscription($parent_student_id)
	{
		$parent_subscription = (new \yii\db\Query())
				->select(['*'])
				->from('parent_subscription')
				->where(['parent_student_id' => $parent_student_id])
				->all();
		if(empty($parent_subscription)){
			return;
		}else{
			$models=ParentSubscription::find()->where(['parent_student_id'=>$parent_student_id])->all();
			foreach($models as $v){
				$v->delete();
			}		
		}
	}
	
}
