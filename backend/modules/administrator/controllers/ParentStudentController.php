<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\ParentStudent;
use app\modules\administrator\models\School;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SchoolClass;
use app\modules\administrator\models\ParentProfile;
use app\modules\administrator\models\ParentStudentSearch;
use app\modules\administrator\models\StudentFilterForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\controllers\DefaultController;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;

/**
 * ParentStudentController implements the CRUD actions for ParentStudent model.
 */
class ParentStudentController extends DefaultController
{

   public function actionIndex()
    {
        /*$searchModel = new ParentStudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/
        return $this->redirect(['parent-profile/index']);
    }
	
	public function actionStudent($id) {
		//get students of parent
		$count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM parent_student WHERE parent_id=:parent_id', [':parent_id' => $id])->queryScalar();
		$parent=$this->findParentById($id);
		$dataProvider = new SqlDataProvider([
			'sql' => 'SELECT student.id as student_id, student.surname as student_surname, student.name as student_name, student.lastname as
			student_lastname 
			FROM student
			JOIN parent_student ON student.id = parent_student.student_id
			WHERE parent_student.parent_id = :parent_id Order by student_surname',
            'params' => ['parent_id' => $id],
	    	'totalCount' => (int)$count,
		]);
		$studentFilterForm = new StudentFilterForm();
        $studentFilterForm->school_years = SchoolYear::find()->all();
		if ($studentFilterForm->load(\Yii::$app->request->post())) {
			$studentFilterForm->getFilteredStudents($id);
			return $this->render('student', [
        		'studentFilterForm' => $studentFilterForm,
            	'dataProvider' => $dataProvider,
				'parent' => $parent,
        	]);
		} else {
			return $this->render('student', [
        		'studentFilterForm' => $studentFilterForm,
            	'dataProvider' => $dataProvider,
				'parent' => $parent,
        	]);
		}
    }

    public function actionAdd($student_id, $parent_id) {
    	//echo $student_id.' '.$parent_id;
    	if (isset($student_id)) {
    		$parentStudent = new ParentStudent();
    		$parentStudent->parent_id = $parent_id;
    		$parentStudent->student_id = $student_id;
    		if ($parentStudent->validate()) {
    			$parentStudent->save();
    		} else {
    			\Yii::$app->getSession()->setFlash('error', 'Учащийся уже добавлен');
    		}
    	}
        $this->redirect(['student', 'id' => $parent_id]);
    }

    public function actionRemove($student_id, $parent_id) {
    	if (isset($student_id)) {
    		$parentStudent = ParentStudent::findOne(['student_id' => $student_id, 'parent_id' => $parent_id]);
    		if (!$parentStudent->delete()) {
    			\Yii::$app->getSession()->setFlash('error', 'Невозможно удалить учащегося, т.к. на него существует подписка');
    		}
    		$this->redirect(['student', 'id' => $parent_id]);
    	} else {
    		$this->redirect(['student', 'id' => $parent_id]);
    	}
    }

    public function actionGetClasses() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId && $params->data->schoolId) {
            $schoolClasses = SchoolClass::find()
                ->join('JOIN', 'school_history', 'school_class.school_id = school_history.id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_history.school_id = :school_id AND school_year.id = :school_year_id', [':school_id' => $params->data->schoolId, 'school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schoolClasses;
        } else {
            return null;
        }
    }

    public function actionGetSchools() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId) {
            $schools = School::find()
                ->join('JOIN', 'school_history', 'school.id = school_history.school_id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_year.id = :school_year_id', ['school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schools;
        } else {
            return null;
        }
    }
	
	protected function findModel($id)
    {
		$model = ParentStudent::find()->where(['parent_id'=>$id])->one();
			return $model;
    }
	
	protected function findParentById($id)
    {
		$parent = ParentProfile::find()->where(['id'=>$id])->one();
			return $parent;
    }

}
