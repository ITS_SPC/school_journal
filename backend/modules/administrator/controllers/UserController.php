<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\User;
use app\modules\administrator\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\models\SignupForm;
use app\modules\administrator\controllers\DefaultController;
use app\modules\administrator\models\Manager;
use app\modules\administrator\models\Worker;
use app\modules\administrator\models\Administrator;
use app\modules\administrator\models\ParentProfile;
use app\modules\administrator\models\ParentPhone;
use app\modules\administrator\models\ParentStudent;
use app\modules\administrator\models\ParentSubscription;
use app\modules\administrator\models\AuthAssignment;
use yii\data\SqlDataProvider;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends DefaultController
{
	 /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/
	
	public function actionProfile($id)
    {
        $model = $this->findModel($id);
		$profile = AuthAssignment::find()->where(['user_id'=>$id])->one();
			
		switch($profile->item_name){
					case 'administrator':
						$this->redirect(['update-administrator-profile', 'id' => $id]);
						break;
					case 'parent':
						$this->redirect(['update-parent-profile', 'id' => $id]);
						break;
					case 'manager':
						$this->redirect(['update-manager-profile', 'id' => $id]);
						break;
					case 'worker':
						$this->redirect(['update-worker-profile', 'id' => $id]);
						break;
		}
    }
	
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        	if ($model->resetPassword) {
        		$model->password = $model->resetPassword;
        	}
        	$model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		$profile = AuthAssignment::find()->where(['user_id'=>$id])->one();
		
		switch($profile->item_name){
			case 'administrator':
			//delete user from administrator table
			if(Yii::$app->user->identity->id==$id){
				\Yii::$app->getSession()->setFlash('error', 'Нельзя удалять свой профайл!');
				return $this->redirect(['index']);
			}else{
				$this->getAdministratorById($id)->delete();
			}
				break;
			case 'parent':
			//delete user from parent table
				//delete all parent subscriptions from parent_subscription table
				$this->deleteParentSubscription($id);
				//delete all items of parent from parent_student table
				$this->deleteParentStudent($id);
				//delete all phones of this parent
				$this->deleteParentPhone($id);
				//delete parent from parent table
 				$this->getParentById($id)->delete();
				break;
			case 'manager':
			//delete user from manager table if it has not school
				$manager=$this->getManagerById($id);
				if($manager->getManagersSchools($id)==NULL){
					$manager->delete();
				}else{
					Yii::$app->getSession()->setFlash('error', 'Невозможно удалить менеджера');
				return $this->redirect('index');
				}
				break;
			case 'worker':
			//delete user from worker table
				$this->getWorkerById($id)->delete();
				break;
		}
		//delete user from user table by id
		$this->findModel($id)->delete();
		//delete user from auth_assignment table by id
		$profile->delete();

	return $this->redirect(['index']); 
} 


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionSignup()
    {
		$model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
				$userRole = Yii::$app->authManager->getRole($model->role_name);
				Yii::$app->authManager->assign($userRole, $user->getId());

				switch($model->role_name){
					case 'administrator':
						//save empty admin profile
						$admin = new Administrator();
						$admin->id = $user->id;
						$admin->save();
						$this->redirect(['update-administrator-profile', 'id' => $user->id]);
						break;
					case 'parent':
					//save empty parent profile
						$parent = new ParentProfile();
						$parent->id = $user->id;
						$parent->save();
						
						$this->redirect(['update-parent-profile', 'id' => $user->id]);
						break;
					case 'manager':
                        //save empty manager profile
                        $manager = new Manager();
                        $manager->id = $user->id;
                        $manager->save();
						$this->redirect(['update-manager-profile', 'id' => $user->id]);
						break;
					case 'worker':
					//save empty worker profile
						$worker = new Worker();
						$worker->id = $user->id;
						$worker->save();
						$this->redirect(['update-worker-profile', 'id' => $user->id]);
						break;
					}
					return;
				}	
            }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }
	
	public function actionUpdateManagerProfile($id)
    {
        $manager = $this->getManagerById($id);
        if ($manager->load(Yii::$app->request->post()) && $manager->save()) {
            $this->redirect(Yii::$app->urlManager->createUrl("administrator/user"));
        } else {
            return $this->render('create_manager', ['model' => $manager,]);
        }
    }

    protected function getManagerById($id) {
        if (($model = Manager::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionUpdateWorkerProfile($id)
    {
        $worker = $this->getWorkerById($id);
        if ($worker->load(Yii::$app->request->post()) && $worker->save()) {
            $this->redirect(Yii::$app->urlManager->createUrl("administrator/user"));
        } else {
            return $this->render('create_worker', ['model' => $worker,]);
        }
    }

    protected function getWorkerById($id) {
        if (($model = Worker::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionUpdateAdministratorProfile($id)
    {
        $admin = $this->getAdministratorById($id);
        if ($admin->load(Yii::$app->request->post()) && $admin->save()) {
            $this->redirect(Yii::$app->urlManager->createUrl("administrator/user"));
        } else {
            return $this->render('create_administrator', ['model' => $admin,]);
        }
    }

    protected function getAdministratorById($id) {
        if (($model = Administrator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionUpdateParentProfile($id)
    {
        $parent = $this->getParentById($id);
		
        if ($parent->load(Yii::$app->request->post()) && $parent->save()) {
            $this->redirect(Yii::$app->urlManager->createUrl("administrator/user"));
        } else {
            return $this->render('create_parent_profile', ['model' => $parent,]);
        }
    }

    protected function getParentById($id) {
        if (($model = ParentProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function deleteParentSubscription($id){
		$parent_subscription = (new \yii\db\Query())
				->select(['*'])
				->from('parent_subscription')
				->leftJoin('parent_phone','parent_phone.id=parent_subscription.parent_phone_id')
				->where(['parent_id' => $id])
				->all();
		if(empty($parent_subscription)){
			return;
		}else{
			$models=ParentSubscription::find()->joinWith(['parentPhone'])->where(['parent_id'=>$id])->all();
			foreach($models as $v){
				$v->delete();
			} 
		}	
	}	
	
	protected function deleteParentStudent($id){
		$parent_student = (new \yii\db\Query())
				->select(['*'])
				->from('parent_student')
				->where(['parent_id' => $id])
				->all();
		if(empty($parent_student)){
			return;
		}else{
			$models=ParentStudent::find()->where(['parent_id'=>$id])->all();
			foreach($models as $v){
				$v->delete();
			}
		}		
	}	
	
	protected function deleteParentPhone($id){
		$parent_phone = (new \yii\db\Query())
				->select(['*'])
				->from('parent_phone')
				->where(['parent_id' => $id])
				->all();
		if(empty($parent_phone)){
			return;
		}else{
			$models=ParentPhone::find()->where(['parent_id'=>$id])->all();
			foreach($models as $v){
				$v->delete();
			}		
		}
	}

}
