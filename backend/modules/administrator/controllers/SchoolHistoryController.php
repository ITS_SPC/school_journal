<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\SchoolHistory;
use app\modules\administrator\models\SchoolHistorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\controllers\DefaultController;

/**
 * SchoolHistoryController implements the CRUD actions for SchoolHistory model.
 */
class SchoolHistoryController extends DefaultController
{
    /*public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all SchoolHistory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SchoolHistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new SchoolHistory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SchoolHistory();
		
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
        }else{
			if(isset($model->school_id)){
				Yii::$app->getSession()->setFlash('error', 'Такая школа уже существует');
			}
			return $this->render('create', [
                'model' => $model,
			]);
        }
    }

    /**
     * Updates an existing SchoolHistory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SchoolHistory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(!$this->findModel($id)->delete()){
            Yii::$app->getSession()->setFlash('error', 'Невозможно удалить школу');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the SchoolHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SchoolHistory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
