<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\Partner;
use app\modules\administrator\models\UploadForm;
use app\modules\administrator\models\PartnerSearch;
use yii\web\Controller;
use app\modules\administrator\controllers\DefaultController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\helpers\Translit;

/**
 * PartnerController implements the CRUD actions for Partner model.
 */
class PartnerController extends DefaultController
{
    /**
     * Lists all Partner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartnerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partner model.
     * @param integer $id
     * @return mixed
     */
    /*public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/

    /**
     * Creates a new Partner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$model_1 = new UploadForm;
				
        $model = new Partner();
		
		 if (Yii::$app->request->isPost) {
			
			if($_FILES['UploadForm']['size']['myFile']==0){	
				if ($model->load(Yii::$app->request->post()) && $model->save()) {
					return $this->redirect(['index']);
				} else {
					return $this->render('create', ['model' => $model,]);
				}
			} else{	
            $model_1->myFile = UploadedFile::getInstance($model_1, 'myFile');
			//added transliteration
			
			//Добавлено для подстановки partnername до пробела в logo
			$translit_name=Translit::transliteration(preg_replace("/(\s.*)$/", "", $_POST['Partner']['partnername']));
						
				if ($model_1->upload($translit_name)) {
					//added translit_name saving with extension
					$model->load(Yii::$app->request->post());
					$model_1->name=$translit_name. '.' . $model_1->myFile->extension;
					$model->logo=$model_1->translit_name. '.' . $model_1->myFile->extension;
					$model->save();
	
					return $this->redirect(['index']);
				}
			}	
        } 
		return $this->render('upload', ['model' => $model, 'model_1' => $model_1]);
    }

    /**
     * Updates an existing Partner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {	
		$model = $this->findModel($id);
		$model_1 = new UploadForm();

		if($_FILES['UploadForm']['size']['myFile']==0){	
			$file_name = $model->logo;
			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				rename('../frontend/uploads/' . $file_name, '../frontend/uploads/' . $model->logo);
					return $this->redirect(['index']);
			} else {
				return $this->render('update', [
					'model' => $model,'model_1' => $model_1]);
			}
		} else{
			if (Yii::$app->request->isPost) {
				$model_1->myFile = UploadedFile::getInstance($model_1, 'myFile');
                $translit_name=Translit::transliteration(preg_replace("/(\s.*)$/", "", $model->partnername));
				if($model_1->upload($translit_name)){
					$file_name = $model->logo;
					$model->load(Yii::$app->request->post());
					rename('../frontend/uploads/' . $file_name, '../frontend/uploads/' . $model->logo);
					if($model->logo!==''){
						unlink('../frontend/uploads/' . $model->logo);
					}
					$model->logo=$model_1->translit_name. '.' . $model_1->myFile->extension;
					$model->save();
				return $this->redirect(['index']);
				}else{
					return $this->render('update', [
					'model' => $model,'model_1' => $model_1]);
				}
			}
		}
    }

    /**
     * Deletes an existing Partner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if($this->findModel($id)->logo!==''){
		//added file deleting
		unlink('../frontend/uploads/' . $this->findModel($id)->logo);
        }
		$this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Partner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Partner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
		
        if (($model = Partner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
