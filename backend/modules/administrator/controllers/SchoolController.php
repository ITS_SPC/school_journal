<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\School;
use app\modules\administrator\models\SchoolHistory;
use app\modules\administrator\models\SchoolClass;
use app\modules\administrator\models\SchoolStudent;
use app\modules\administrator\models\StudentMark;
use app\modules\administrator\models\StudentSubject;
use app\modules\administrator\models\Student;
use app\modules\administrator\models\ParentStudent;
use app\modules\administrator\models\ParentSubscription;
use app\modules\administrator\models\SchoolSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\controllers\DefaultController;


/**
 * SchoolController implements the CRUD actions for School model.
 */
class SchoolController extends DefaultController
{
   /* public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all School models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SchoolSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single School model.
     * @param integer $id
     * @return mixed
     */
/*     public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    } */

    /**
     * Creates a new School model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new School();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing School model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing School model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		//delete associated item from school_history table
		$this->deleteSchoolHistory($id);
		
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the School model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return School the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = School::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function deleteSchoolHistory($id)
	{
		$school_history = (new \yii\db\Query())
				->select(['*'])
				->from('school_history')
				->where(['school_id' => $id])
				->all();
		if(empty($school_history)){
			return;
		}else{
 			$models=SchoolHistory::find()->where(['school_id'=>$id])->all();
			foreach($models as $v){
				$this->deleteSchoolClass($v->id);
				$v->delete();
			}		
		} 
	}
	
	protected function deleteSchoolClass($school_history_id)
	{
		$school_class=(new \yii\db\Query())
				->select(['*'])
				->from('school_class')
				->where(['school_id' => $school_history_id])
				->all();
		if(empty($school_class)){
			return;
		}else{
			$models=SchoolClass::find()->where(['school_id'=>$school_history_id])->all();
			foreach($models as $v){
				$this->deleteSchoolStudent($v->id);
				$v->delete();
			}	
		}
	}
	
	/**
	 * Deletes school student from school_student table 
	 * by school class id and 
	 * call methods that deletes 
	 * associated items from student_mark,
	 * student_subject and student tables
	 */
	protected function deleteSchoolStudent($school_class_id)
	{
		$school_student = (new \yii\db\Query())
				->select(['*'])
				->from('school_student')
				->where(['school_class_id' => $school_class_id])
				->all();
		if(empty($school_student)){
			return;
		}else{
 			$models=SchoolStudent::find()->where(['school_class_id'=>$school_class_id])->all();
			foreach($models as $v){
				//delete all associated items from student_mark table
				$this->deleteStudentMarks($v->id);
				
				//delete all associated items from student_subject table
				$this->deleteStudentSubject($v->id);
				
				//delete student from student table
				$v->delete();
				$this->deleteStudent($v->personal_number);
				
			}		
		} 
	}
	
	/**
	 * Deletes student from student table 
	 * by student personal number from 
	 * school_student table
	 */
	protected function deleteStudent($personal_number)
	{
		$student = (new \yii\db\Query())
				->select(['*'])
				->from('student')
				->where(['id' => $personal_number])
				->all();
		if(empty($student)){
			return;
		}else{
			$models=Student::find()->where(['id' => $personal_number])->all();
			foreach($models as $v){
				$this->deleteParentStudent($v->id);
				$v->delete();
			}
		}
	}	
	
	/**
	 * Deletes parent-student relation by deleting
	 * item from parent_student table by student id
	 */
	protected function deleteParentStudent($student_id)
	{
		$parent_student = (new \yii\db\Query())
				->select(['*'])
				->from('parent_student')
				->where(['student_id' => $student_id])
				->all();
		if(empty($parent_student)){
			return;
		}else{
			$models=ParentStudent::find()->where(['student_id'=>$student_id])->all();
			foreach($models as $v){
				$this->deleteParentSubscription($v->id);
				$v->delete();
			}		
		}											
	}
	/**
	 * Finds the ParentSubscription model where parent_student_id
	 * associated with similar item of parent_student table
	 */
	protected function deleteParentSubscription($parent_student_id)
	{
		$parent_subscription = (new \yii\db\Query())
				->select(['*'])
				->from('parent_subscription')
				->where(['parent_student_id' => $parent_student_id])
				->all();
		if(empty($parent_subscription)){
			return;
		}else{
			$models=ParentSubscription::find()->where(['parent_student_id'=>$parent_student_id])->all();
			foreach($models as $v){
				$v->delete();
			}		
		}
	}
	
	/**
	 * Deletes student marks from student_mark table by school_student_id
	 */
	protected function deleteStudentMarks($school_student_id)
	{
	$student_mark = (new \yii\db\Query())
				->select(['*'])
				->from('student_mark')
				->where(['student_id' => $school_student_id])
				->all();
		if(empty($student_mark)){
			return;
		}else{
			$models=StudentMark::find()->where(['student_id'=>$school_student_id])->all();
			foreach($models as $v){
				$v->delete();
			}
		}
	}
	
	/**
	 * Deletes student subjects from student_subject table by school_student_id
	 */
	protected function deleteStudentSubject($school_student_id){
		$student_subject = (new \yii\db\Query())
				->select(['*'])
				->from('student_subject')
				->where(['student_id' => $school_student_id])
				->all();
		if(empty($student_subject)){
			return;
		}else{
			$models=StudentSubject::find()->where(['student_id'=>$school_student_id])->all();
			foreach($models as $v){
				$v->delete();
			}
		}
	}
	
	
}
