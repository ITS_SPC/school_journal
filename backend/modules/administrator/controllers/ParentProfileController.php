<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\ParentProfile;
use app\modules\administrator\models\ParentPhone;
use app\modules\administrator\models\ParentProfileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\controllers\DefaultController;
use yii\data\ActiveDataProvider;

/**
 * ParentProfileController implements the CRUD actions for ParentProfile model.
 */
class ParentProfileController extends DefaultController
{
    /**
     * Lists all ParentProfile models.
     * @return mixed
     */
/*    public function actionIndex()
    {
        $searchModel = new ParentProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/

    public function actionIndex()
    {
        if(isset($_POST['phone'])){
            $phone_number=$_POST['phone'];
            $parent_phone=ParentPhone::find()->where(['phone_number'=>$phone_number])->one();
            $dataProvider = new ActiveDataProvider([
                'query' => ParentProfile::find()->where(['id'=>$parent_phone->parent_id])
                ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);

        }else{
            $searchModel = new ParentProfileSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

/*  public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate()
    {
        $model = new ParentProfile();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	*/
    protected function findModel($id)
    {
        if (($model = ParentProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
