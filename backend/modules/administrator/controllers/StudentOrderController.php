<?php

namespace app\modules\administrator\controllers;

use app\modules\administrator\models\StudentSubject;
use app\modules\administrator\models\SubjectStudentsFilterForm;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SchoolClass;
use app\modules\administrator\models\School;
use app\modules\administrator\models\Subject;
use app\modules\administrator\models\StudentOrderForm;
use app\modules\administrator\models\SchoolStudent;
use app\modules\administrator\controllers\DefaultController;
use yii\helpers\ArrayHelper;

class StudentOrderController extends DefaultController
{
    public function actionIndex() {

    	$studentFilterForm = new SubjectStudentsFilterForm();
        $studentFilterForm->school_years = SchoolYear::find()->all();
        $studentFilterForm->subjects = Subject::find()->all();
        $studentOrderForm = new StudentOrderForm();

        if ($studentFilterForm->load(\Yii::$app->request->post())) {
        	$studentOrderForm->getFilteredSubjectStudents($studentFilterForm);
		}

		return $this->render('index', [
        	'studentFilterForm' => $studentFilterForm,
        	'studentOrderForm' => $studentOrderForm
        ]);
    }

    public function actionSetOrder() {
    	$params = json_decode(file_get_contents('php://input'));
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	if (isset($params->data->classStudents) && isset($params->data->subjectStudents)) {
    		$classStudents = $params->data->classStudents;
    		$subjectStudents = $params->data->subjectStudents;
    		//remove all order rows
    		for ($i = 0, $maxi = count($classStudents); $i < $maxi; $i++) {
    			StudentSubject::deleteAll(['student_id' => $classStudents[$i]->student, 'subject_id' => $classStudents[$i]->subject]);
    		}
    		//save subject students
    		for ($i = 0, $maxi = count($subjectStudents); $i < $maxi; $i++) {
    			$studentSubject = new StudentSubject();
    			$studentSubject->student_id = $subjectStudents[$i]->student;
    			$studentSubject->subject_id = $subjectStudents[$i]->subject;
    			$studentSubject->order_number = $subjectStudents[$i]->order;
    			$studentSubject->save();
    		}
    		return $params->data;
    	} else {
    		return false;
    	}
    }

    public function actionGetClasses() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId && $params->data->schoolId) {
            $schoolClasses = SchoolClass::find()
                ->join('JOIN', 'school_history', 'school_class.school_id = school_history.id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_history.school_id = :school_id AND school_year.id = :school_year_id', [':school_id' => $params->data->schoolId, 'school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schoolClasses;
        } else {
            return null;
        }
    }

    public function actionGetSchools() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId) {
            $schools = School::find()
                ->join('JOIN', 'school_history', 'school.id = school_history.school_id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_year.id = :school_year_id', ['school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schools;
        } else {
            return null;
        }
    }

}
