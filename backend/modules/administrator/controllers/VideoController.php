<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\Video;
use app\modules\administrator\models\UploadForm;
use app\modules\administrator\models\VideoSearch;
use app\modules\administrator\models\PartnerSearch;
use app\modules\administrator\models\SchoolPartnerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\helpers\Translit;
use app\modules\administrator\controllers\DefaultController;

/**
 * VideoController implements the CRUD actions for Video model.
 */
class VideoController extends DefaultController
{
    /**
     * Lists all Video models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VideoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Displays a single Video model.
     * @param integer $id
     * @return mixed
     */
    /*public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/

    /**
     * Creates a new Video model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	public function actionUpload()
    {
        $model = new UploadForm();
		//added
		$model_1 = new Video();
		
        if (Yii::$app->request->isPost) {
            $model->myFile = UploadedFile::getInstance($model, 'myFile');
			
            if($model->myFile==null){
                Yii::$app->getSession()->setFlash('error', 'Выберите файл');
                return $this->redirect(['upload']);
            } 
               
			//added transliteration
			$translit_name=Translit::transliteration($model->myFile->baseName);
			
				if ($model->upload($translit_name)) {
					//added translit_name saving with extension
					$model_1->load(Yii::$app->request->post());
					$model_1->name=$model->translit_name. '.' . $model->myFile->extension;
					$model_1->save();
					return $this->redirect(['index']);
				}
			} 
		return $this->render('upload', ['model' => $model, 'model_1' => $model_1]);
	}
    

    /**
     * Updates an existing Video model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		$file_name = $model->name;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			//added
				rename('../frontend/uploads/' . $file_name, '../frontend/uploads/' . $model->name);
		   return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Video model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

	//added {file deleting}

    public function actionDelete($id)
    {
		//added file deleting
		unlink('../frontend/uploads/' . $this->findModel($id)->name);
			
        $this->findModel($id)->delete();
		return $this->redirect(['index']);
    }

    /**
     * Finds the Video model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Video the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Video::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
