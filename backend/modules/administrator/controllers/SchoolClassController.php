<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\SchoolClass;
use app\modules\administrator\models\SchoolClassSearch;
use app\modules\administrator\models\SchoolStudent;
use app\modules\administrator\models\StudentMark;
use app\modules\administrator\models\StudentSubject;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\controllers\DefaultController;

/**
 * SchoolClassController implements the CRUD actions for SchoolClass model.
 */
class SchoolClassController extends DefaultController
{
   /* public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all SchoolClass models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SchoolClassSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new SchoolClass model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SchoolClass();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }else{
            if(isset($model->school_id)){
                Yii::$app->getSession()->setFlash('error', 'Такой класс уже существует');
            }
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SchoolClass model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SchoolClass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->deleteSchoolStudent($id);

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SchoolClass model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolClass the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SchoolClass::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function deleteSchoolStudent($school_class_id)
    {
        $school_student = (new \yii\db\Query())
                ->select(['*'])
                ->from('school_student')
                ->where(['school_class_id' => $school_class_id])
                ->all();
        if(empty($school_student)){
            return;
        }else{
            $models=SchoolStudent::find()->where(['school_class_id'=>$school_class_id])->all();
            foreach($models as $v){
                $this->deleteStudentMarks($v->id);
                $this->deleteStudentSubject($v->id);
                $v->delete();
            }       
        } 
    }
    /**
     * Deletes student marks from student_mark table by school_student_id
     */
    protected function deleteStudentMarks($school_student_id)
    {
    $student_mark = (new \yii\db\Query())
                ->select(['*'])
                ->from('student_mark')
                ->where(['student_id' => $school_student_id])
                ->all();
        if(empty($student_mark)){
            return;
        }else{
            $models=StudentMark::find()->where(['student_id'=>$school_student_id])->all();
            foreach($models as $v){
                $v->delete();
            }
        }
    }
    
    /**
     * Deletes student subjects from student_subject table by school_student_id
     */
    protected function deleteStudentSubject($school_student_id)
    {
        $student_subject = (new \yii\db\Query())
                ->select(['*'])
                ->from('student_subject')
                ->where(['student_id' => $school_student_id])
                ->all();
        if(empty($student_subject)){
            return;
        }else{
            $models=StudentSubject::find()->where(['student_id'=>$school_student_id])->all();
            foreach($models as $v){
                $v->delete();
            }
        }
    }
}
