<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SchoolYearSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\controllers\DefaultController;

/**
 * SchoolYearController implements the CRUD actions for SchoolYear model.
 */
class SchoolYearController extends DefaultController
{
    /**
     * Lists all SchoolYear models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SchoolYearSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new SchoolYear model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	public function actionCreate()
    {
        $model = new SchoolYear();
		
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
        } else {
			return $this->render('create', [
                'model' => $model,
			]);
        }
    }
	
    /**
     * Updates an existing SchoolYear model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SchoolYear model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!$this->findModel($id)->delete()) {
            Yii::$app->getSession()->setFlash('error', 'Невозможно удалить, запись привязана к школе');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the SchoolYear model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolYear the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SchoolYear::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
