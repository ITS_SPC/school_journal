<?php

namespace app\modules\administrator\controllers;

use Yii;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\modules\administrator\models\StudentFilterForm;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SchoolClass;
use app\modules\administrator\models\School;
use app\modules\administrator\models\ParentSubscription;
use app\modules\administrator\models\SystemStatistic;
use app\modules\administrator\models\Administrator;

use app\modules\administrator\models\Message;
use yii\db\Query;

use yii\helpers\ArrayHelper;
use linslin\yii2\curl;

class DefaultController extends Controller {
	public function behaviors() {
		return [
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                    	'allow' => true,
                    	'roles' => ['administrator'],
                    ],
                ],
            ],
        ];
	}
    public function actionIndex() {
        $systemStatistic = new SystemStatistic();
        $systemStatistic->getSystemStatistic();
        $profile = Administrator::find()
            ->where(['id' => Yii::$app->user->identity->id])
            ->one();

        return $this->render('index', [
            'systemStatistic' => $systemStatistic,
            'profile' => $profile
        ]);
    }

    /*public function actionStudentFilter() {

        $studentFilterForm = new StudentFilterForm();
        $studentFilterForm->school_years = SchoolYear::find()->all();

        if ($studentFilterForm->load(\Yii::$app->request->post())) {
            return $this->render('_filter_form', [
                'studentFilterForm' => $studentFilterForm
            ]);
        } else {
            return $this->render('_filter_form', [
                'studentFilterForm' => $studentFilterForm
            ]);
        }
    }*/

    /*public function actionGetClasses() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId && $params->data->schoolId) {
            $schoolClasses = SchoolClass::find()
                ->join('JOIN', 'school_history', 'school_class.school_id = school_history.id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_history.school_id = :school_id AND school_year.id = :school_year_id', [':school_id' => $params->data->schoolId, 'school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schoolClasses;
        } else {
            return null;
        }
    }*/

    /*public function actionGetSchools() {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId) {
            $schools = School::find()
                ->join('JOIN', 'school_history', 'school.id = school_history.school_id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_year.id = :school_year_id', ['school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schools;
        } else {
            return null;
        }
    }*/

    /*public function actionSend() {
        $currentDate = date("Y-m-d");
        $twoDays = strtotime('-1 days');
        $twoDaysBackDate = date('Y-m-d', $twoDays);
        echo $twoDaysBackDate;
        foreach (ParentSubscription::find()->where('subscription_start <= :currentDate AND subscription_end >= :currentDate AND payment_status = 1', ['currentDate' => $currentDate])->batch(2) as $subscriptions) {
            
            foreach ($subscriptions as $subscription) {
                $message = new Message();

                $studentSendArray = array();
                $studentSendArray['subjects'] = array();

                $message->phoneNumber = str_replace(['(', ')', '+', '-', ' '], '', $subscription->parentPhone->phone_number);
                $studentMarks = (new Query())
                    ->select(['student.name', 'student_mark.mark', 'subject.abbreviation'])
                    ->from('parent_student')
                    ->join('LEFT JOIN', 'student', 'student.id = parent_student.student_id')
                    ->join('LEFT JOIN', 'school_student', 'school_student.personal_number = student.id')
                    ->join('LEFT JOIN', 'student_mark', 'student_mark.student_id = school_student.id')
                    ->join('LEFT JOIN', 'subject', 'subject.id = student_mark.subject_id')
                    ->where('student_mark.mark_date <= :currentDate AND student_mark.mark_date >= :twoDaysBackDate AND parent_student.id = :parent_student_id', ['currentDate' => $currentDate, 'twoDaysBackDate' => $twoDaysBackDate, 'parent_student_id' => $subscription->parent_student_id])
                    ->orderBy('student.name')
                    ->all();
                for ($i = 0, $maxi = count($studentMarks); $i < $maxi; $i++) {
                    $studentSendArray['name'] = $studentMarks[$i]['name'];
                    $studentSendArray['subjects'][$studentMarks[$i]['abbreviation']] = array();
                }
                for ($i = 0, $maxi = count($studentMarks); $i < $maxi; $i++) {
                    array_push($studentSendArray['subjects'][$studentMarks[$i]['abbreviation']], $studentMarks[$i]['mark']);
                }
                $message->messageText .= iconv("UTF-8", "CP1251", $studentSendArray['name']."\n");
                foreach ($studentSendArray['subjects'] as $key => $subject) {
                    $message->messageText .= iconv("UTF-8", "CP1251", $key).": ";
                    $message->messageText .= iconv("UTF-8", "CP1251", str_replace([',']," ",implode(' ', $subject)));
                    $message->messageText .= "\n";
                }
                if ($message->validate()) {
                    $curl = new curl\Curl();
                    $curl->setOption(CURLOPT_HEADER, 0);
                    $curl->setOption(CURLOPT_TIMEOUT, 10);
                    $curl->setOption(CURLOPT_RETURNTRANSFER, 1);
                    $curl->setOption(CURLOPT_POST, 1);
                    $response = $curl->setOption(
                        CURLOPT_POSTFIELDS, 
                        http_build_query(array(
                            'Http_username' => urlencode('Arma_92'),
                            'Http_password' => urlencode('123456'),
                            'Phone_list' => $message->phoneNumber,
                            'Message' => $message->messageText
                        )
                    ))->post('http://www.websms.ru/http_in5.asp');
                    $u = trim($response);
                    $arr_id = [];
                    preg_match("/message_id\s*=\s*[0-9]+/i", $u, $arr_id['message_id'] );
                    preg_match("/message_phone\s*=\s*[0-9]+/i", $u, $arr_id['message_phone'] );
                    preg_match("/error_num\s*=\s*(.*)+/i", $u, $arr_id['error_num'] );
                    print_r($arr_id);
                    print_r($response);
                }
            }
        }
    }*/

    /*protected function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}*/
}
