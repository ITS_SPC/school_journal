<?php

namespace app\modules\administrator\controllers;

use Yii;
use app\modules\administrator\models\SchoolStudent;
use app\modules\administrator\models\SchoolStudentSearch;
use app\modules\administrator\models\StudentMark;
use app\modules\administrator\models\StudentSubject;
use app\modules\administrator\models\Subject;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\administrator\controllers\DefaultController;

use app\modules\administrator\models\StudentFilterForm;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SchoolClass;
use app\modules\administrator\models\School;

/**
 * SchoolStudentController implements the CRUD actions for SchoolStudent model.
 */
class SchoolStudentController extends DefaultController
{
/*    public function actionIndex()
    {
        //print_r("<pre>");print_r($_POST);print_r("</pre>");
        $studentFilterForm = new StudentFilterForm();
        $studentFilterForm->school_years = SchoolYear::find()->all();
        if($studentFilterForm->load(\Yii::$app->request->post())) {
            $studentFilterForm->getStudentsOfClass();
            return $this->render('index', [
                'studentFilterForm' => $studentFilterForm,
            ]);
        }else {
            return $this->render('index', [
                'studentFilterForm' => $studentFilterForm,
            ]);
        }
    }*/

    public function actionIndex($school_year_id=null,$school_id=null,$school_class_id=null)
    {
        $studentFilterForm = new StudentFilterForm();

        if(($_POST[StudentFilterForm][school_year_id]!=null)&&
                ($_POST[StudentFilterForm][school_id]!=null)&&
                ($_POST[StudentFilterForm][school_class_id]!=null)) 
            {
                $studentFilterForm->school_years = SchoolYear::find()->all();
                if($studentFilterForm->load(\Yii::$app->request->post())) {
                    $studentFilterForm->getStudentsOfClass();
                    return $this->render('index', [
                    'studentFilterForm' => $studentFilterForm,
                    ]);
                }else{
                    return $this->render('index', [
                        'studentFilterForm' => $studentFilterForm,
                    ]);
                }
            }elseif(($school_year_id!=null)&&($school_id!=null)&&($school_class_id!=null)){
                $studentFilterForm->school_year_id=$school_year_id;
                $studentFilterForm->school_id=$school_id;
                $studentFilterForm->school_class_id=$school_class_id;
                $studentFilterForm->school_years = SchoolYear::find()->all();
                $studentFilterForm->getStudentsOfClass();
                return $this->render('index', [
                        'studentFilterForm' => $studentFilterForm,
                    ]);
            }else{
                $studentFilterForm->school_years = SchoolYear::find()->all();
                if($studentFilterForm->load(\Yii::$app->request->post())) {
                    $studentFilterForm->getStudentsOfClass();
                    return $this->render('index', [
                        'studentFilterForm' => $studentFilterForm,
                    ]);
                 }else{
                     return $this->render('index', [
                        'studentFilterForm' => $studentFilterForm,
                    ]);
                }
            }
    }    

    public function actionGetClasses() 
    {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId && $params->data->schoolId) {
            $schoolClasses = SchoolClass::find()
                ->join('JOIN', 'school_history', 'school_class.school_id = school_history.id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_history.school_id = :school_id AND school_year.id = :school_year_id', [':school_id' => $params->data->schoolId, 'school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schoolClasses;
        } else {
            return null;
        }
    }

    public function actionGetSchools() 
    {
        $params = json_decode(file_get_contents('php://input'));
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($params->data->schoolYearId) {
            $schools = School::find()
                ->join('JOIN', 'school_history', 'school.id = school_history.school_id')
                ->join('JOIN', 'school_year', 'school_year.id = school_history.school_year_id')
                ->where('school_year.id = :school_year_id', ['school_year_id' => $params->data->schoolYearId])
                ->all();
            return $schools;
        } else {
            return null;
        }
    }


    /**
     * Creates a new SchoolStudent model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($school_class_id)
    {
        $model = new SchoolStudent();

        $model->school_class_id=$school_class_id;

        $school_year_id=$model->schoolClass->school->school_year_id;
        $school_id=$model->schoolClass->school->school_id;
        $school_class_id=$model->school_class_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index','school_year_id'=>$school_year_id,
                        'school_id'=>$school_id,'school_class_id'=> $school_class_id]);
        }else{
            if(isset($model->personal_number)){
                Yii::$app->getSession()->setFlash('error', 'Такой ученик уже существует');
            }
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SchoolStudent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $school_year_id=$model->schoolClass->school->school_year_id;
        $school_id=$model->schoolClass->school->school_id;
        $school_class_id=$model->school_class_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index','school_year_id'=>$school_year_id,
                        'school_id'=>$school_id,'school_class_id'=> $school_class_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SchoolStudent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->deleteStudentMarks($id);
        $this->deleteStudentSubject($id);


        $model=$this->findModel($id);
        $school_year_id=$model->schoolClass->school->school_year_id;
        $school_id=$model->schoolClass->school->school_id;
        $school_class_id=$model->school_class_id;
        $model->delete();

        return $this->redirect(['index','school_year_id'=>$school_year_id,
                        'school_id'=>$school_id,'school_class_id'=> $school_class_id]);
    }

    /**
     * Finds the SchoolStudent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolStudent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SchoolStudent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function deleteStudentMarks($school_student_id)
    {
    $student_mark = (new \yii\db\Query())
                ->select(['*'])
                ->from('student_mark')
                ->where(['student_id' => $school_student_id])
                ->all();
        if(empty($student_mark)){
            return;
        }else{
            $models=StudentMark::find()->where(['student_id'=>$school_student_id])->all();
            foreach($models as $v){
                $v->delete();
            }
        }
    }
    
    /**
     * Deletes student subjects from student_subject table by school_student_id
     */
    protected function deleteStudentSubject($school_student_id)
    {
        $student_subject = (new \yii\db\Query())
                ->select(['*'])
                ->from('student_subject')
                ->where(['student_id' => $school_student_id])
                ->all();
        if(empty($student_subject)){
            return;
        }else{
            $models=StudentSubject::find()->where(['student_id'=>$school_student_id])->all();
            foreach($models as $v){
                $v->delete();
            }
        }
    }
}