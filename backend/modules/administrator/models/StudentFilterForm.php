<?php
namespace app\modules\administrator\models;

use yii\base\Model;
use Yii;
use app\modules\administrator\models\Student;
use app\modules\administrator\models\SchoolStudent;
use app\modules\administrator\models\Subject;
use app\modules\administrator\models\StudentMark;
use yii\data\ActiveDataProvider;
use app\modules\administrator\models\School;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SchoolClass;

/**
 * Signup form
 */
class StudentFilterForm extends Model
{
    public $school_id;
    public $school_class_id;
    public $school_year_id;
    public $subject;
    public $subjectAll;

    public $school_years;
    public $filteredStudentsDataProvider;
    public $filteredStudentsOfClassDataProvider;
    public $filteredStudentsMarksDataProvider;
    public $currentSchool;
    public $currentSchoolYear;
    public $currentClass;
    public $currentSubject;


    public function attributeLabels() {
        return [
            'school_id' => 'Школа',
            'school_class_id' => 'Класс',
            'school_year_id' => 'Учебный год',
            'subject' => 'Предмет'
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['school_id', 'school_class_id', 'school_year_id'], 'required'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    /*public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }*/

    public function getFilteredStudents($parent_id = null) {
        if ($this->validate()) {
            $this->filteredStudentsDataProvider = new ActiveDataProvider([
                'query' => Student::find()
                    ->leftJoin('school_student', '`school_student`.`personal_number` = `student`.`id`')
                    ->leftJoin('school_class', '`school_student`.`school_class_id` = `school_class`.`id`')
                    ->leftJoin('school_history', '`school_class`.`school_id` = `school_history`.`id`')
                    ->leftJoin('school_year', '`school_history`.`school_year_id` = `school_year`.`id`')
                    ->where('school_year.id = :school_year_id AND school_history.school_id = :school_id AND school_class.id = :school_class_id AND student.id NOT IN (SELECT student_id FROM parent_student WHERE parent_id = :parent_id)',
                        [
                            'school_year_id' => $this->school_year_id,
                            'school_id' => $this->school_id, 
                            'school_class_id' => $this->school_class_id,
                            'parent_id' => $parent_id
                        ])
                    ->orderBy([
                        'student.surname' => SORT_ASC,
                        'student.name' => SORT_ASC
                    ]),
                'pagination' => [
                    'pageSize' => 50,
                ],    
            ]);
            //get current filter for form
            $this->currentSchoolYear = SchoolYear::find()->where(['id' => $this->school_year_id])->one();
            $this->currentSchool = School::find()->where(['id' => $this->school_id])->one();
            $this->currentClass = SchoolClass::find()->where(['id' => $this->school_class_id])->one();
        } else {
            return null;
        }
    }

    public function getStudentsOfClass(){
        if ($this->validate()) {
            $this->filteredStudentsOfClassDataProvider = new ActiveDataProvider([
                'query' => SchoolStudent::find()
                    ->leftJoin('school_class', '`school_student`.`school_class_id` = `school_class`.`id`')
                    ->leftJoin('school_history', '`school_class`.`school_id` = `school_history`.`id`')
                    ->leftJoin('school_year', '`school_history`.`school_year_id` = `school_year`.`id`')
                    ->where(['school_year.id' => $this->school_year_id, 'school_history.school_id' => $this->school_id, 
                        'school_class.id' => $this->school_class_id])
                    ->groupBy(['school_student.id']),
                'pagination' => [
                    'pageSize' => 50,
                ],
            ]);

            $this->currentSchoolYear = SchoolYear::find()->where(['id' => $this->school_year_id])->one();
            $this->currentSchool = School::find()->where(['id' => $this->school_id])->one();
            $this->currentClass = SchoolClass::find()->where(['id' => $this->school_class_id])->one();
            $this->currentSubject = Subject::find()->where(['id' => $this->subject])->one();

        } else{
            return null;
        }    
    }

    public function getStudentMarks($student_id, $subject_id){
        if ($this->validate()) {
            $this->filteredStudentsMarksDataProvider = new ActiveDataProvider([
                'query' => StudentMark::find()
                        ->where(['student_id'=>$student_id,'subject_id'=>$subject_id])
                    ->orderBy([
                        'student_id' => SORT_ASC,
                    ]),    
            ]);
        } else{
            return null;
        }    
    }
}