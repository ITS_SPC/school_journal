<?php

namespace app\modules\administrator\models;

use Yii;
use app\modules\administrator\models\StudentSubject;
use app\modules\administrator\models\StudentMark;
/**
 * This is the model class for table "subject".
 *
 * @property integer $id
 * @property string $name
 * @property string $abbreviation
 *
 * @property StudentMark[] $studentMarks
 * @property StudentSubject[] $studentSubjects
 * @property SchoolStudent[] $students
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'abbreviation'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['abbreviation'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Предмет',
            'name' => 'Название',
            'abbreviation' => 'Аббревиатура',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentMarks()
    {
        return $this->hasMany(StudentMark::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentSubjects()
    {
        return $this->hasMany(StudentSubject::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(SchoolStudent::className(), ['id' => 'student_id'])->viaTable('student_subject', ['subject_id' => 'id']);
    }


    public function beforeDelete() 
    {
        if (parent::beforeSave($insert)) {
            if ((StudentSubject::find()->where(['subject_id' => $this->id])->exists()) || (StudentMark::find()->where(['subject_id' => $this->id])->exists())) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
