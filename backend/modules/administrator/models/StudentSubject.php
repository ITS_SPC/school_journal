<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "student_subject".
 *
 * @property integer $student_id
 * @property integer $subject_id
 * @property integer $order_number
 *
 * @property SchoolStudent $student
 * @property Subject $subject
 */
class StudentSubject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_subject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'subject_id', 'order_number'], 'required'],
            [['student_id', 'subject_id', 'order_number'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_id' => 'ID учащегося',
            'subject_id' => 'ID предмета',
            'order_number' => 'Порядковый номер',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(SchoolStudent::className(), ['id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

     public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(StudentSubject::find()->where(['student_id'=>$this->student_id])
                ->andWhere(['subject_id'=>$this->subject_id])->exists()){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
}
