<?php

namespace app\modules\administrator\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\administrator\models\SchoolStudent;

/**
 * SchoolStudentSearch represents the model behind the search form about `app\modules\administrator\models\SchoolStudent`.
 */
class SchoolStudentSearch extends SchoolStudent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'personal_number', 'school_class_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SchoolStudent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
               'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'personal_number' => $this->personal_number,
            'school_class_id' => $this->school_class_id,
        ]);

        return $dataProvider;
    }
}
