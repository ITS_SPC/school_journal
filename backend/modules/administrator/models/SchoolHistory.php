<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "school_history".
 *
 * @property integer $id
 * @property integer $school_id
 * @property integer $school_year_id
 * @property integer $subscription_rate_id
 *
 * @property SchoolClass[] $schoolClasses
 * @property SchoolYear $schoolYear
 * @property School $school
 * @property SubscriptionRate $subscriptionRate
 */
class SchoolHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'school_year_id', 'subscription_rate_id'], 'required'],
            [['school_id', 'school_year_id', 'subscription_rate_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'Школа',
            'school_year_id' => 'Учебный год',
            'subscription_rate_id' => 'Тариф',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolClasses()
    {
        return $this->hasMany(SchoolClass::className(), ['school_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolYear()
    {
        return $this->hasOne(SchoolYear::className(), ['id' => 'school_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(School::className(), ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionRate()
    {
        return $this->hasOne(SubscriptionRate::className(), ['id' => 'subscription_rate_id']);
    }
	
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if(SchoolHistory::find()->where(['school_id'=>$this->school_id])->andWhere(['school_year_id'=>$this->school_year_id])->exists()){
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
	}
	
	function beforeDelete(){
        if( $this->schoolClasses !== array() ) return false;
        return parent::beforeDelete();
    }
}
