<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "parent_subscription".
 *
 * @property integer $id
 * @property string $subscription_start
 * @property string $subscription_end
 * @property integer $parent_phone_id
 * @property integer $payment_status
 * @property double $amount
 * @property integer $parent_student_id
 *
 * @property ParentStudent $parentStudent
 * @property ParentPhone $parentPhone
 */
class ParentSubscription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parent_subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_start', 'subscription_end', 'parent_phone_id', 'payment_status', 'amount', 'parent_student_id'], 'required'],
            [['subscription_start', 'subscription_end'], 'safe'],
            [['parent_phone_id', 'payment_status', 'parent_student_id'], 'integer'],
            [['amount'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscription_start' => 'Начало подписки',
            'subscription_end' => 'Окончание подписки',
            'parent_phone_id' => 'Телефон родителя',
            'payment_status' => 'Статус оплаты',
            'amount' => 'Счет',
            'parent_student_id' => 'Родственное отношение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentStudent()
    {
        return $this->hasOne(ParentStudent::className(), ['id' => 'parent_student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentPhone()
    {
        return $this->hasOne(ParentPhone::className(), ['id' => 'parent_phone_id']);
    }
}
