<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "manager".
 *
 * @property integer $id
 * @property string $surname
 * @property string $name
 * @property string $lastname
 * @property string $phone
 *
 * @property User $id0
 * @property School[] $schools
 */
class Manager extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manager';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['id', 'surname', 'name', 'lastname', 'phone'], 'required'],
            [['id'], 'integer'],

            [['surname', 'name', 'lastname'], 'filter', 'filter' => 'trim'],
            [['surname', 'name', 'lastname'], 'string', 'min' => 2, 'max' => 200],

            [['phone'], 'string', 'max' => 200],
            [['phone'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Фамилия',
            'name' => 'Имя',
            'lastname' => 'Отчество',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchools()
    {
        return $this->hasMany(School::className(), ['manager_id' => 'id']);
    }
	
	public function getManagersSchools($id)
	{
		return School::find()->where(['manager_id'=>$id])->one();
	}
}
