<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "school_student".
 *
 * @property integer $id
 * @property integer $personal_number
 * @property integer $school_class_id
 *
 * @property SchoolClass $schoolClass
 * @property Student $personalNumber
 * @property StudentMark[] $studentMarks
 * @property StudentSubject[] $studentSubjects
 * @property Subject[] $subjects
 */
class SchoolStudent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['personal_number', 'school_class_id'], 'required'],
            [['personal_number', 'school_class_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'personal_number' => 'Учащийся',
            'school_class_id' => 'Класс',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolClass()
    {
        return $this->hasOne(SchoolClass::className(), ['id' => 'school_class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonalNumber()
    {
        return $this->hasOne(Student::className(), ['id' => 'personal_number']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentMarks()
    {
        return $this->hasMany(StudentMark::className(), ['student_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentSubjects()
    {
        return $this->hasMany(StudentSubject::className(), ['student_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasMany(Subject::className(), ['id' => 'subject_id'])->viaTable('student_subject', ['student_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(SchoolStudent::find()->where(['personal_number'=>$this->personal_number])
                                ->andWhere(['school_class_id'=>$this->school_class_id])->exists()){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
}
