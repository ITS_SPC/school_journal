<?php
namespace app\modules\administrator\models;

use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SubjectStudentsFilterForm extends Model
{
    public $school_id;
    public $school_class_id;
    public $school_year_id;
    public $subject_id;

    public $school_years;
    public $filteredStudentsDataProvider;
    public $currentSchool;
    public $currentSchoolYear;
    public $currentClass;
    public $subjects;


    public function attributeLabels() {
        return [
            'school_id' => 'Школа',
            'school_class_id' => 'Класс',
            'school_year_id' => 'Учебный год',
            'subject_id' => 'Предмет'
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['school_id', 'school_class_id', 'school_year_id', 'subject_id'], 'required'],
        ];
    }
}