<?php
namespace app\modules\administrator\models;

use yii\base\Model;
use Yii;
use app\modules\administrator\models\Student;
use yii\data\ActiveDataProvider;
use app\modules\administrator\models\School;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SchoolClass;
use app\modules\administrator\models\StudentSubject;

/**
 * Signup form
 */
class StudentOrderForm extends Model
{

    public $subjectStudents = array();
    public $classStudents = array();
    public $subjectId;

    public $currentSchoolYear;
    public $currentSchool;
    public $currentClass;
    public $subject;


    /*public function attributeLabels() {
        return [
            'school_id' => 'Школа',
            'school_class_id' => 'Класс',
            'school_year_id' => 'Учебный год',
            'subject_id' => 'Предмет'
        ];
    }*/
    /**
     * @inheritdoc
     */
    /*public function rules() {
        return [
            [['school_id', 'school_class_id', 'school_year_id', 'subject_id'], 'required'],
        ];
    }*/

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    /*public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }*/

    public function getFilteredSubjectStudents($studentFilterForm) {
        if ($this->validate()) {
            $this->subjectId = $studentFilterForm->subject_id;
            $this->subjectStudents = StudentSubject::find()
                ->join('JOIN', 'school_student', 'school_student.id = student_subject.student_id')
                ->where('school_student.school_class_id = :school_class_id AND student_subject.subject_id = :subject_id', [':school_class_id' => $studentFilterForm->school_class_id, ':subject_id' => $studentFilterForm->subject_id])
                ->orderBy('order_number')
                ->all();
            $this->classStudents = SchoolStudent::find()
                ->where('school_student.school_class_id = :school_class_id AND school_student.id NOT IN (SELECT student_subject.student_id FROM student_subject JOIN school_student ON school_student.id = student_subject.student_id WHERE  school_student.school_class_id = :school_class_id AND student_subject.subject_id = :subject_id)', [
                    ':school_class_id' => $studentFilterForm->school_class_id,
                    ':school_class_id' => $studentFilterForm->school_class_id,
                    ':subject_id' => $studentFilterForm->subject_id
                ])
                ->all();
            $this->getSubjectStudentsArray();
            $this->getClassStudentsArray();
            //get current filter for form
            $this->currentSchoolYear = SchoolYear::find()->where(['id' => $studentFilterForm->school_year_id])->one();
            $this->currentSchool = School::find()->where(['id' => $studentFilterForm->school_id])->one();
            $this->currentClass = SchoolClass::find()->where(['id' => $studentFilterForm->school_class_id])->one();
            $this->subject = Subject::find()->where(['id' => $studentFilterForm->subject_id])->one();
        }
    }
    public function getSubjectStudentsArray() {
        if (count($this->subjectStudents) && ($this->subjectId)) {
            $students = array();
            for ($i = 0, $maxi = count($this->subjectStudents); $i < $maxi; $i++) {
                $students[] = array('content' => $this->subjectStudents[$i]->student->personalNumber->surname." ".$this->subjectStudents[$i]->student->personalNumber->name, 'options'=>['id' => $this->subjectStudents[$i]->student_id, 'data-subject' => $this->subjectId]);
            }
            $this->subjectStudents = $students;
        }
    }
    public function getClassStudentsArray() {
        if (count($this->classStudents) && ($this->subjectId)) {
            $students = array();
            for ($i = 0, $maxi = count($this->classStudents); $i < $maxi; $i++) {
                $students[] = array('content' => $this->classStudents[$i]->personalNumber->surname." ".$this->classStudents[$i]->personalNumber->name, 'options'=>['id' => $this->classStudents[$i]->id, 'data-subject' => $this->subjectId]);
            }
            asort($students);
            $this->classStudents = $students;
        }
    }
}