<?php

namespace app\modules\administrator\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\administrator\models\SchoolHistory;

/**
 * SchoolHistorySearch represents the model behind the search form about `app\modules\administrator\models\SchoolHistory`.
 */
class SchoolHistorySearch extends SchoolHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'school_year_id', 'subscription_rate_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SchoolHistory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
               'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'school_id' => $this->school_id,
            'school_year_id' => $this->school_year_id,
            'subscription_rate_id' => $this->subscription_rate_id,
        ]);

        return $dataProvider;
    }
}
