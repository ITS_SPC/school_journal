<?php
namespace app\modules\administrator\models;

use Yii;
use yii\base\Model;

class Message extends Model {

	public $phoneNumber;
	public $messageText = "";

	/**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['phoneNumber', 'messageText'], 'required'],
        ];
    }
}
