<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "school_partner".
 *
 * @property integer $id
 * @property integer $number
 * @property string $name
 * @property string $address
 * @property string $description
 * @property string $logo
 * @property integer $publish
 */
class SchoolPartner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'integer'],
            [['name'], 'required'],
            [['name', 'address', 'description'], 'string'],
            [['logo'], 'string', 'max' => 255],
			[['publish'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'name' => 'Название школы',
            'address' => 'Адрес',
            'description' => 'Информация',
            'logo' => 'Логотип',
            'publish' => 'Опубликовано',
        ];
    }
}
