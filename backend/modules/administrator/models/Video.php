<?php

namespace app\modules\administrator\models;

use yii\base\Model;
use Yii;
use Yii\web\UploadedFile;
use app\modules\administrator\models\UploadForm;


/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property string $name
 */
class Video extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
			[['description'], 'safe'],
			[['publish'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
			'description' => 'Описание',
			'publish' => 'Опубликовано',
        ];
    }
}
