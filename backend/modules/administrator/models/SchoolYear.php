<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "school_year".
 *
 * @property integer $id
 * @property string $start_year
 * @property string $end_year
 *
 * @property SchoolHistory[] $schoolHistories
 */
class SchoolYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_year', 'end_year'], 'required'],
            [['start_year', 'end_year'], 'date', 'format' => 'php:Y'],
            [['start_year', 'end_year'], 'validateSchoolYear'],
            [['start_year', 'end_year'], 'unique', 'targetAttribute' => ['start_year', 'end_year']]
        ];
    }

    public function validateSchoolYear() {
        if ($this->start_year >= $this->end_year) {
            $this->addError('start_year', 'Недопустимый диапазон годов.');
        } else {
            $date1 = date_create("2013-03-15");
            $diff = date_diff(date_create($this->start_year), date_create($this->end_year));
            if ($diff->i != 1) {
                $this->addError('start_year', 'Недопустимый диапазон годов.');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Учебный год',
            'start_year' => 'Начало года',
            'end_year' => 'Конец года',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolHistories()
    {
        return $this->hasMany(SchoolHistory::className(), ['school_year_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if(SchoolHistory::find()->where(['school_year_id'=>$this->id])->exists()) { 
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
