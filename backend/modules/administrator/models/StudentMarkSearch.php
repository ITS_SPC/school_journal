<?php

namespace app\modules\administrator\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\administrator\models\StudentMark;

/**
 * StudentMarkSearch represents the model behind the search form about `app\modules\administrator\models\StudentMark`.
 */
class StudentMarkSearch extends StudentMark
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'subject_id', 'student_id'], 'integer'],
            [['mark_date', 'mark'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StudentMark::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
/*            'pagination' => [
               'pageSize' => 50,
            ],*/
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'subject_id' => $this->subject_id,
            'student_id' => $this->student_id,
            'mark_date' => $this->mark_date,
        ]);

        $query->andFilterWhere(['like', 'mark', $this->mark]);

        return $dataProvider;
    }
}
