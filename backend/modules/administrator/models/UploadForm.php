<?php

namespace app\modules\administrator\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\base\Component;

class UploadForm extends Model
{

    /**
     * @var UploadedFile
     */
    public $myFile;
    public $name;
    public $description;
	public $translit_name;
		
	public $isNewRecord=true;
	
	public static function tableName()
    {
        return 'video';
    }

    public function rules()
    {
        return [
			[['name', 'description'], 'safe'],
            [['myFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'avi, mpeg, flv, mkv, wmv, mp4, mpeg4, png, jpg, jpeg, bmp'],
        ];
    }
	
	public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
			'myFile' => 'Файл',
        ];
    }

    public function upload($translit_name)
    {	
		$tmp=$translit_name;
		while(file_exists("../frontend/uploads/{$translit_name}.{$this->myFile->extension}")){
			$count++;
			$translit_name="{$tmp}_{$count}";	
		}
		$this->translit_name=$translit_name;
		
        if ($this->validate()) {
        $this->myFile->saveAs('../frontend/uploads/' . $this->myFile->name=$translit_name . '.' . $this->myFile->extension);
            return true;
        } else {
            return false;
        }
    }

}
?>