<?php

namespace app\modules\administrator\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\administrator\models\ParentSubscription;

/**
 * ParentSubscriptionSearch represents the model behind the search form about `app\modules\administrator\models\ParentSubscription`.
 */
class ParentSubscriptionSearch extends ParentSubscription
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_phone_id', 'payment_status', 'parent_student_id'], 'integer'],
            [['subscription_start', 'subscription_end'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ParentSubscription::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
               'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'subscription_start' => $this->subscription_start,
            'subscription_end' => $this->subscription_end,
            'parent_phone_id' => $this->parent_phone_id,
            'payment_status' => $this->payment_status,
            'amount' => $this->amount,
            'parent_student_id' => $this->parent_student_id,
        ]);

        return $dataProvider;
    }
}
