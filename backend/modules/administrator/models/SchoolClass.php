<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "school_class".
 *
 * @property integer $id
 * @property string $name
 * @property integer $school_id
 *
 * @property SchoolHistory $school
 * @property SchoolStudent[] $schoolStudents
 */
class SchoolClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'school_id'], 'required'],
            [['school_id'], 'integer'],
            [['name'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'school_id' => 'Школа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(SchoolHistory::className(), ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolStudents()
    {
        return $this->hasMany(SchoolStudent::className(), ['school_class_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(SchoolClass::find()->where(['school_id'=>$this->school_id])->andWhere(['name'=>$this->name])->exists()){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
}
