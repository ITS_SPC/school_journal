<?php

namespace app\modules\administrator\models;

use Yii;

/**
 * This is the model class for table "subscription_rate".
 *
 * @property integer $id
 * @property double $value
 *
 * @property SchoolHistory[] $schoolHistories
 */
class SubscriptionRate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'required'],
            [['value'], 'unique'],
            [['value'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Величина тарифа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolHistories()
    {
        return $this->hasMany(SchoolHistory::className(), ['subscription_rate_id' => 'id']);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            if(SchoolHistory::find()->where(['subscription_rate_id'=>$this->id])->exists()) { 
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
