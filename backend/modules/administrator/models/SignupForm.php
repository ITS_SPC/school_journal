<?php
namespace app\modules\administrator\models;

use app\modules\administrator\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
	public $role_name;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['role_name', 'required'],
            ['role_name', 'string'],
            ['username', 'unique', 'targetClass' => 'app\modules\administrator\models\User', 'message' => 'Пользователь с таким именем уже существует.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => 'app\modules\administrator\models\User', 'message' => 'Такой адресс эл.почты уже существует.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }
	
	public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
			'email' => 'Почта',
			'password' => 'Пароль',
			'role_name' => 'Роль в системе',
        ];
    }
	
	
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
