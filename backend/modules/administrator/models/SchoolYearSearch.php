<?php

namespace app\modules\administrator\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\administrator\models\SchoolYear;

/**
 * SchoolYearSearch represents the model behind the search form about `backend\modules\administrator\models\SchoolYear`.
 */
class SchoolYearSearch extends SchoolYear
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['start_year', 'end_year'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SchoolYear::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'pagination' => [
               'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'start_year' => $this->start_year,
            'end_year' => $this->end_year,
        ]);

        return $dataProvider;
    }
}
