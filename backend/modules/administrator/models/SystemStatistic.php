<?php
namespace app\modules\administrator\models;

use yii\base\Model;
use Yii;
use app\modules\administrator\models\Student;
use app\modules\administrator\models\School;
use app\modules\administrator\models\ParentSubscription;
use app\modules\administrator\models\ParentProfile;
use app\modules\administrator\models\Manager;
use app\modules\administrator\models\Worker;
use yii\data\ActiveDataProvider;

/**
 * SystemStatistic
 */
class SystemStatistic extends Model {
    public $totalSchools;
    public $totalStudents;
    public $totalParents;
    public $totalManagers;
    public $totalWorkers;
    public $totalActiveSubscriptions;


    public function attributeLabels() {
        return [
            'totalSchools' => 'Школы',
            'totalStudents' => 'Учащиеся',
            'totalParents' => 'Родители',
            'totalManagers' => 'Менеджеры',
            'totalWorkers' => 'Рабочие',
            'totalActiveSubscriptions' => 'Активные подписки'
        ];
    }

    public function getSystemStatistic() {
        $this->totalSchools = School::find()
            ->count();
        $this->totalStudents = Student::find()
            ->count();
        $this->totalParents = ParentProfile::find()
            ->count();
        $this->totalManagers = Manager::find()
            ->count();
        $this->totalWorkers = Worker::find()
            ->count();
        $this->totalActiveSubscriptions = ParentSubscription::find()
            ->where(['payment_status' => 1])
            ->count();
    }
}