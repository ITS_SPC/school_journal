<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use backend\controllers\PartnerController;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Видео';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Загрузить видео', ['video/upload'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
			'description',
			'publish:boolean',

            ['class' => 'yii\grid\ActionColumn',
			  'controller' => 'video',
			  'template' => '{update} {delete}',
			],		
        ],
    ]); 
	?>
</div>	

