<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Video */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="video-form">

    <?php $form = ActiveForm::begin((['options' => ['enctype' => 'multipart/form-data']]) ); ?>
	
    <?= $form->field($model_1, 'description')->textarea(['rows' => 6]) ?>
	
	<?= $form->field($model, 'myFile')->fileInput() ?>
	
	<?= $form->field($model_1, 'publish')->checkbox(['checked' => true]) ?>
		
	<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Загрузить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	
    <?php ActiveForm::end(); ?>

</div>
