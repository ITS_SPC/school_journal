<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administrator\models\SchoolPartnerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Школы партнеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-partner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить школу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'number',
            'name:ntext',
            'address:ntext',
            'description:ntext',
            'logo',
            'publish:boolean',

            ['class' => 'yii\grid\ActionColumn',
			'controller' => 'school-partner',
			'template' => '{update} {delete}',			
			],
			
        ],
    ]); ?>

</div>
