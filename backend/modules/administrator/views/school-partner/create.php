<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SchoolPartner */

$this->title = 'Create School Partner';
$this->params['breadcrumbs'][] = ['label' => 'School Partners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-partner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
