<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use app\modules\administrator\models\ParentPhone;
use app\modules\administrator\models\ParentStudent;
use app\modules\administrator\models\ParentProfile;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentSubscription */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parent-subscription-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subscription_start')->widget(DatePicker::className(),[
        'language' => 'ru',
        'clientOptions'=>[
            'format'=>'yyyy-mm-dd']
        ]) ?>

    <?= $form->field($model, 'subscription_end')->widget(DatePicker::className(),[
        'language' => 'ru',
        'clientOptions'=>[
            'format'=>'yyyy-mm-dd']
        ]) ?>

    <?= $form->field($model, 'parent_phone_id')->dropDownList(ArrayHelper::map(ParentPhone::find()->all(), 'id', 'phone_number')) ?>

    <?= $form->field($model, 'payment_status')->checkbox(['checked' => true]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'parent_student_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(ParentStudent::find()->all(), 'id', 
            function($model){
                return $model->student->surname." "
                    .$model->student->name."-"
                    .$model->parentProfile->surname." "
                    .$model->parentProfile->name." "
                    .$model->parentProfile->lastname;
        }),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите пару школьник-родитель...'],
            'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
