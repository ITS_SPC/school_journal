<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentSubscription */

$this->title = 'Добавить подписку';
$this->params['breadcrumbs'][] = ['label' => 'История подписок', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-subscription-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
