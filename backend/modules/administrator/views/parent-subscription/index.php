<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\administrator\models\ParentPhone;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administrator\models\ParentSubscriptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История подписок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-subscription-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить подписку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'subscription_start',
            'subscription_end',
			//'parent_phone_id',
            [
             'attribute'=> 'parent_phone_id',
             'label' => 'Телефон родителя',
             'filter' => ArrayHelper::map(ParentPhone::find()->all(), 'id', 'phone_number'),
             'value' => 'parentPhone.phone_number',
            ],
            'payment_status:boolean',
            'amount',
           // 'parent_student_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
