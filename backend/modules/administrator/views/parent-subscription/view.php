<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentSubscription */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'История подписок', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-subscription-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'subscription_start',
            'subscription_end',
            //'parent_phone_id',
            [
            'label' => 'Телефон родителя',
            'value' => $model->parentPhone->phone_number,
            ],
            'payment_status:boolean',
            'amount',
            [
            'label' => 'ID родственного отношения',
            'value' => $model->parent_student_id,
            ],
            [
            'label' => 'Школьник',
            'value' => $model->parentStudent->student->surname." "
                    .$model->parentStudent->student->name." "
                    .$model->parentStudent->student->lastname,
            ],
            [
            'label' => 'Родитель',
            'value' => $model->parentStudent->parentProfile->surname." "
                    .$model->parentStudent->parentProfile->name." "
                    .$model->parentStudent->parentProfile->lastname,
            ],
        ],
    ]) ?>

</div>
