<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentSubscription */

$this->title = 'Обновить подписку';
$this->params['breadcrumbs'][] = ['label' => 'Подписки', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="parent-subscription-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
