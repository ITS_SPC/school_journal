<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
//use yii\widgets\Alert;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
	
    NavBar::begin([
        'brandLabel' => 'Школьный журнал',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

	$menuItems[] = ['label' => 'Управление контентом', 'url' => ['index'],
        'items'=>[
            ['label'=>'Видео','url' => ['video/index']],
			['label'=>'Партнеры','url' => ['partner/index']],
            ['label'=>'Школы-партнеры','url' => ['school-partner/index']],
			['label'=>'Контакты','url' => ['contact/index']],
		],
	];
	$menuItems[] = ['label' => 'Управление школами', 'url' => ['index'],
        'items'=>[
            ['label'=>'Школы','url' => ['school/index']],
			['label'=>'Учебные года','url' => ['school-year/index']],
            ['label'=>'Тарифы','url' => ['subscription-rate/index']],
			['label'=>'Школы в учебном году','url' => ['school-history/index']],
			['label'=>'Классы','url' => ['school-class/index']],
			['label'=>'Школьники','url' => ['student/index']],
			['label'=>'Учащиеся школы','url' => ['school-student/index']],
			['label'=>'Оценки','url' => ['student-mark/index']],
            ['label'=>'Предметы','url' => ['subject/index']],
            ['label'=>'Порядковый номер учащегося в предмете','url' => ['student-order/index']]
		],
	];
	$menuItems[] = ['label' => 'Управление родителями', 'url' => ['index'],
            'items'=>[
               ['label'=>'Родители','url' => ['parent-profile/index']],
			   ['label'=>'Подписки','url' => ['parent-subscription/index']],
            ],
    ];
	$menuItems[] = ['label' => 'Управление пользователями', 'url' => ['index'],
            'items'=>[
                ['label'=>'Пользователи','url' => ['user/index']],
            ],
    ];
	
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Вход', 'url' => ['/site/login']];
    } else {
        $menuItems[] = [
            'label' => 'Выход',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Школьный журнал <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
