<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Partner */

$this->title = 'Обновить: ' . ' ' . $model->partnername;
$this->params['breadcrumbs'][] = ['label' => 'Партнеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->partnername, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="partner-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'model_1' =>$model_1,
    ]) ?>

</div>
