<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentStudent */

$this->title = 'Create Parent Student';
$this->params['breadcrumbs'][] = ['label' => 'Parent Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-student-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
