<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administrator\models\ParentStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $parent->id.' '.$parent->surname.' '.$parent->name.' '.$parent->lastname.'';
$this->params['breadcrumbs'][] = ['label' => 'Список родителей', 'url' => ['parent-profile/index']];
$this->params['breadcrumbs'][] = $parent->surname.' '.$parent->name.' '.$parent->lastname.'';
?>
<div class="parent-student-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
			'label' => 'ID',
			'value' => 'student_id',
			],
			[
			'label' => 'Фамилия',
			'value' => 'student_surname',
			],
			[
			'label' => 'Имя',
			'value' => 'student_name',
			],
			[
			'label' => 'Отчество',
			'value' => 'student_lastname',
			],
			[
				'class' => \yii\grid\ActionColumn::className(),
				'template' => '{remove}',
				'buttons' => [
					'remove' => function ($url, $model) {
						return Html::a('<span class="glyphicon glyphicon-remove-sign"></span>', $url, [
							'title' => "Удалить",
							'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?')
						]);
					}],
					'urlCreator' => function ($action, $model, $key, $index) {
						if ($action === 'remove') {
							return Url::toRoute(['remove', 'student_id' => $model['student_id'], 'parent_id' => \Yii::$app->getRequest()->getQueryParam('id')]);
						}
					},
					'contentOptions' => ['class' => 'text-center']
            ]
        ],
    ]); ?>

    <?= $this->render('_student_filter_form', [
        'studentFilterForm' => $studentFilterForm,
		'parent' => $parent,
    ]) ?>
	
</div>
