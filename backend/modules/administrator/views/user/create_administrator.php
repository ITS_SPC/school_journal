<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\Administrator */

$this->title = 'Информация об администраторе' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Администратор';
?>
<div class="administrator-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_administrator_form', [
        'model' => $model,
    ]) ?>

</div>
