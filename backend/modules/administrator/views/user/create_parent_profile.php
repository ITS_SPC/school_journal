<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentProfile */

$this->title = 'Информация о родителе' . ' ' . $model->surname;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Родитель';
?>
<div class="parent-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_parent_profile_form', [
        'model' => $model,
    ]) ?>

</div>
