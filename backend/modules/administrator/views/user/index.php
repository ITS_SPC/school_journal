<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\administrator\models\AuthAssignment;
use app\modules\administrator\models\AuthItem;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administrator\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить пользователя', ['signup'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'email:email',
			[
			//'attribute' => 'id',
			'label' => 'Роль',
            //'filter' => ArrayHelper::map(AuthItem::find()->all(), 'name', 'name'),
			//'filter' => ArrayHelper::map(AuthItem::find()->asArray()->all(), 'name', 'name'),
			'value' => 'authAssignment.item_name',
			],

            ['class' => 'yii\grid\ActionColumn',
			'controller' => 'user',
			'template' => '{update} {profile} {delete}',
			'buttons' => [
                'profile' => function ($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-user"></span>', $url);
                },
				],
			],
        ],
    ]); ?>

</div>
