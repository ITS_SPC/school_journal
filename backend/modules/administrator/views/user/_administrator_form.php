<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\PhoneMaskAsset;

PhoneMaskAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\Administrator */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
   '$("document").ready(function(){ 
        $("#administrator-phone").mask("+7(999)999-9999", { completed: function () { $(".hidden").slideDown("slow"); } });
    });'
);
?>

<div class="administrator-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Добавить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
