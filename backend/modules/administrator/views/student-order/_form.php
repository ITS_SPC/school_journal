<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\sortable\Sortable;
use backend\assets\StudentOrderAsset;

StudentOrderAsset::register($this);
/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentStudent */
/* @var $form yii\widgets\ActiveForm */
?>

<div id="studentOrderApp" ng-app="studentOrderApp" class="student-order-form">
	<div ng-controller="OrderController">
		<div class="row">
	        <div class="col-md-4">
	            <h2 style="margin-top: 0">Учащиеся в журнале</h2>
	        </div>
	        <div class="col-md-4">
	            <span class="saving-order" style="display:none"></span>
	        </div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			<div ng-repeat="(key,val) in alerts" class="alert {{key}}">
        			<div ng-repeat="msg in val">{{msg}}</div>
    			</div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			<?php if (isset($studentOrderForm->currentSchoolYear)) { ?>
	    			<?= Html::tag('span', $studentOrderForm->currentSchoolYear->start_year.' - '.$studentOrderForm->currentSchoolYear->end_year, ['class'=>'label label-primary']) ?>
	    		<?php } ?>
	    		<?php if (isset($studentOrderForm->currentSchool)) { ?>
	    			<?= Html::tag('span', $studentOrderForm->currentSchool->name, ['class'=>'label label-primary']) ?>
	    		<?php } ?>
	    		<?php if (isset($studentOrderForm->currentClass)) { ?>
	    			<?= Html::tag('span', $studentOrderForm->currentClass->name, ['class'=>'label label-primary']) ?>
	    		<?php } ?>
	    		<?php if (isset($studentOrderForm->subject)) { ?>
	    			<?= Html::tag('span', $studentOrderForm->subject->name, ['class'=>'label label-primary']) ?>
	    		<?php } ?>
    		</div>
    		<br><br>
    	</div>
		<div class="row">
			<div class="col-xs-5">
				<div class="panel panel-primary">
  					<div class="panel-heading">Учащиеся класса</div>
  					<div class="panel-body">
  						<?= 
							Sortable::widget([
					    		'connected'=>true,
					    		'items'=>$studentOrderForm->classStudents,
					    		'options' => ['id' => 'classStudents']/*[
					        	['content'=>'From Item 1'],
					        	['content'=>'From Item 2'],
					        	['content'=>'From Item 3'],
					        	['content'=>'From Item 4'],
					    		]*/
							]);
						?>
  					</div>
				</div>
			</div>
			<div class="col-xs-2 text-center">
				<div class="row">
					<div class="col-xs-12">
						
						<?= Html::submitButton('>>', ['class' => 'btn btn-success', 'ng-click' => 'moveStudentsToSubject()' ]) ?>

					</div>
				</div>
				<div class="row row-margin">
					<div class="col-xs-12">
						
						<?= Html::submitButton('<<', ['class' => 'btn btn-success', 'ng-click' => 'removeStudentsFromSubject()' ]) ?>

					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						
						<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'ng-click' => 'assembleData()' ]) ?>

					</div>
				</div>
			</div>
			<div class="col-xs-5">
				<div class="panel panel-primary">
  					<div class="panel-heading">Обучающиеся предмету</div>
  					<div class="panel-body">
  						<?= 
							Sortable::widget([
				    			'connected'=>true,
				    			'itemOptions'=>['class'=>'alert alert-warning'],
				    			'items'=>$studentOrderForm->subjectStudents,
				    			'options' => ['id' => 'subjectStudents']/*[
				        			['content'=>'Имя_0', 'options'=>['id' => 3]],
				        			['content'=>'Имя_1'],
				        			['content'=>'Имя_2'],
				        			['content'=>'Имя_3'],
				    				]*/
								]); 
						?>
  					</div>
				</div>
			</div>
		</div>
	</div>
</div>
