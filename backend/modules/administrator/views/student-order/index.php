<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Порядковый номер учащегося в предмете';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= $this->title ?></h1>

<?= $this->render('_student_filter_form', [
        'studentFilterForm' => $studentFilterForm,
    ]) ?>

<?=
	$this->render('_form', [
		'studentOrderForm' => $studentOrderForm
	]);
?>
