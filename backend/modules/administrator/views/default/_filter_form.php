<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use backend\assets\StudentFilterAsset;

StudentFilterAsset::register($this);
/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div ng-app="studentFilterApp" class="administrator-default-index">
    <h1>Фильтр учащихся</h1>
    <span class="spinner" style="display:none"></span>

    <?php $form = ActiveForm::begin(); ?>

    <div ng-controller="FilterForm" class="row">
        <div class="col-md-4">
            
            <?= $form->field($studentFilterForm, 'school_year_id')->dropdownList(ArrayHelper::map($studentFilterForm->school_years, 'id',
                            function($model, $defaultValue) {
                                return $model->start_year.' '.$model->end_year;
                            }), ['prompt' => '---Учебный год---','ng-model' => 'schoolYearId']) ?>

        </div>
        <div class="col-md-4">
            
            <?= $form->field($studentFilterForm, 'school_id')->dropdownList(['prompt' => '---Школа---'], ['ng-model' => 'selectedSchool.id', 'ng-options' => 'school.id as school.name for school in schools track by school.id', ]) ?>

        </div>
        <div class="col-md-4">
            
            <?= $form->field($studentFilterForm, 'school_class_id')->dropdownList(['prompt' => '---Класс---'], ['ng-model' => 'selectedClass.id', 'ng-options' => 'class.id as class.name for class in schoolClasses track by class.id', ]) ?>

        </div>

        <div class="form-group">
            <?= Html::submitButton('Найти', ['class' => 'btn btn-success' ]) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
