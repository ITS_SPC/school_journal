<div class="administrator-default-index">
    <h1>Панель администратора</h1>
    <div class="row">
    	<div class="col-md-6">
    		
    		<?= $this->render('_statistic', [
        		'systemStatistic' => $systemStatistic,
    		]) ?>

    	</div>
    	<div class="col-md-6">
    		
    		<?= $this->render('_profile', [
        		'profile' => $profile,
    		]) ?>

    	</div>
    </div>
</div>
