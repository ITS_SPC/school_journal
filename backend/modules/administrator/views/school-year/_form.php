<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SchoolYear */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="school-year-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'start_year')->widget(DatePicker::className(), [
                            'template' => '{addon}{input}',
                            'language' => 'ru',
                            'clientOptions' => [
                                'startView' => 1,
                                'minViewMode' => 2,
                                'maxViewMode' => 1,
                                'format' => 'yyyy',
                                'multidateSeparator' => ' ; ',
                                'todayBtn' => "linked",
                                'clearBtn' => true,
                                'autoclose' => true,
                            ],
                            'options' => ['ng-model' => 'subscriptionForm.subscriptionStart'],
                        ]);
                    ?>

    <?= $form->field($model, 'end_year')->widget(DatePicker::className(), [
                            'template' => '{addon}{input}',
                            'language' => 'ru',
                            'clientOptions' => [
                                'startView' => 1,
                                'minViewMode' => 2,
                                'maxViewMode' => 1,
                                'format' => 'yyyy',
                                'multidateSeparator' => ' ; ',
                                'todayBtn' => "linked",
                                'clearBtn' => true,
                                'autoclose' => true,
                            ],
                            'options' => ['ng-model' => 'subscriptionForm.subscriptionStart'],
                        ]);
                    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
