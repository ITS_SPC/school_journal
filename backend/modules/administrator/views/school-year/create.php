<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SchoolYear */

$this->title = 'Добавить учебный год';
$this->params['breadcrumbs'][] = ['label' => 'Учебные года', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-year-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
