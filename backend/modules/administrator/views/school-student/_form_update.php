<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\administrator\models\Student;
use app\modules\administrator\models\SchoolClass;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SchoolStudent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="school-student-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'personal_number')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Student::find()->all(), 'id',
                 function($model, $defaultValue) {
                     return $model->id." ".$model->surname." ".$model->name." ".$model->lastname;}),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите школьника...'],
            'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'school_class_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(SchoolClass::find()->all(), 'id', 
            function($model){
                return $model->name." "
                    .$model->school->school->name.": "
                    .$model->school->schoolYear->start_year."/"
                    .$model->school->schoolYear->end_year;
            }),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберете класс...'],
            'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
