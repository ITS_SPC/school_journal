<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\assets\StudentFilterAsset;
use yii\grid\GridView;
use yii\helpers\Url;
use app\modules\administrator\models\SchoolStudent;
use app\modules\administrator\models\Student;


StudentFilterAsset::register($this);
/* @var $this yii\web\View */
/* @var $model common\models\ParentProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div ng-app="studentFilterApp" class="administrator-default-index">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-3">
                    <h2 style="margin-top: 0">Фильтр учащихся</h2>
                </div>
                <div class="col-md-3">
                    <span class="spinner" style="display:none"></span>
                </div>
            </div>
        </div>
        <div class="panel-body">
    
    <?php $form = ActiveForm::begin(); ?>

        <div ng-controller="FilterForm" class="row">
            <div class="col-md-4">
            
                <?= $form->field($studentFilterForm, 'school_year_id')->dropdownList(ArrayHelper::map($studentFilterForm->school_years, 'id',
                            function($model, $defaultValue) {
                                return $model->start_year.' - '.$model->end_year;
                            }), ['prompt' => '---Учебный год---','ng-model' => 'schoolYearId']) ?>

            </div>
            <div class="col-md-4">
            
                <?= $form->field($studentFilterForm, 'school_id')->dropdownList(['prompt' => '---Школа---'], ['ng-model' => 'selectedSchool.id', 'ng-options' => 'school.id as school.name for school in schools track by school.id', ]) ?>

            </div>
            <div class="col-md-4">
            
                <?= $form->field($studentFilterForm, 'school_class_id')->dropdownList(['prompt' => '---Класс---'], ['ng-model' => 'selectedClass.id', 'ng-options' => 'class.id as class.name for class in schoolClasses track by class.id', ]) ?>

            </div>
        </div>

        <div class="row text-right">
            <div class="col-md-12">

                <?= Html::submitButton('Найти', ['class' => 'btn btn-success' ]) ?>
            
            </div>
        </div>

    <?php ActiveForm::end(); ?>

    <?php
        if ($studentFilterForm->filteredStudentsOfClassDataProvider)  { ?>

        <p> 
            <?php 

                if(isset($_POST[StudentFilterForm][school_class_id])){
                    $school_class_id=$_POST[StudentFilterForm][school_class_id];
                }else{
                    $school_class_id=\Yii::$app->getRequest()->getQueryParam('school_class_id');
                }
            ?>

            <?= Html::a('Добавить учащегося', ['create','school_class_id'=>$school_class_id], 
                                                ['class' => 'btn btn-success']) ?>
        </p>

            
            <?= Html::tag('span', $studentFilterForm->currentSchoolYear->start_year.' - '.$studentFilterForm->currentSchoolYear->end_year, ['class'=>'label label-primary']) ?>
            <?= Html::tag('span', $studentFilterForm->currentSchool->name, ['class'=>'label label-primary']) ?>
            <?= Html::tag('span', $studentFilterForm->currentClass->name, ['class'=>'label label-primary']) ?>


            <?=  GridView::widget([
                'dataProvider' => $studentFilterForm->filteredStudentsOfClassDataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                    //'attribute'=>'student_id',
                    'label'=>'Учащиеся школы',
                    'value'=> function($model){
                        return $model->personalNumber->surname." "
                            .$model->personalNumber->name." "
                            .$model->personalNumber->lastname;
                    }
                    ],
                    [ 
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    ],
                ],
            ]); ?>

    <?php } ?>
	</div>
	</div>

</div>