<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SchoolStudent */

$this->title = 'Обновить информацию об учащемся '.$model->schoolClass->name.' класса, 
	школы: '.$model->schoolClass->school->school->name.', учебный 
	год: '.$model->schoolClass->school->schoolYear->start_year.'/'
		.$model->schoolClass->school->schoolYear->end_year;
$this->params['breadcrumbs'][] = ['label' => 'Учащиеся школы', 
	'url' => ['index','school_year_id'=>$model->schoolClass->school->school_year_id,
                        'school_id'=>$model->schoolClass->school->school_id,
                        'school_class_id'=> $model->school_class_id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="school-student-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
