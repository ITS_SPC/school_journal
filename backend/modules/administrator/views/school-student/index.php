<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\administrator\models\SchoolStudent;
use app\modules\administrator\models\Subject;


/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administrator\models\StudentMarkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Учащиеся школы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-mark-index">

    <?= $this->render('_student_filter_form', [
        'studentFilterForm' => $studentFilterForm,
    ]) ?>

</div>
