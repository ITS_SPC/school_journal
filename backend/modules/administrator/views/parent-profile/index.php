<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\assets\PhoneMaskAsset;

PhoneMaskAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administrator\models\ParentProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список родителей';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
   '$("document").ready(function(){ 
        $("#parent-phone").mask("+7(999)999-9999", { completed: function () { $(".hidden").slideDown("slow"); } });
    });'
);
?>
<div class="parent-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">Поиск по номеру телефона</div>
                <div class="panel-body text-center">

                    <?= Html::beginForm(['parent-profile/index', 'phone' => $phone], 'post', ['enctype' => 'text']) ?>
                    
                        <?= Html::textInput('phone', '', ['id' => 'parent-phone']) ?>

                        <?= Html::submitButton('Найти', ['class' => 'btn btn-success']) ?>
    
                    <?= Html::endForm() ?>

                </div>
            </div>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'surname',
            'name',
            'lastname',
            'score',
            'address',

            [
                'class' => 'yii\grid\ActionColumn',
			     //parent_student_controller
                'controller' => 'parent-student',
                'template' => '{student}',
                'buttons' => [
                    'student' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', $url, ['title' => "Редактировать учащихся"]);
                    },
				],
			],
        ],
    ]); ?>

</div>
