<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentProfile */

$this->title = 'Добавить родителя';
$this->params['breadcrumbs'][] = ['label' => 'Список родителей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
