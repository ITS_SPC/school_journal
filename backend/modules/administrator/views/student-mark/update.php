<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\StudentMark */
$fio=$model->student->personalNumber->surname." "
.$model->student->personalNumber->name." ".$model->student->personalNumber->lastname;
$this->title = 'Обновить оценку учащемуся: '.$fio.''; 
$this->params['breadcrumbs'][] = ['label' => 'Оценки учащихся', 
    'url' => ['student-mark/index','school_year_id'=>$model->student->schoolClass->school->school_year_id,
                'school_id'=>$model->student->schoolClass->school->school_id,
                'school_class_id'=>$model->student->school_class_id]];
$this->params['breadcrumbs'][] = ['label' => $fio, 'url' => ['student-mark/view', 'id'=>$model->student_id]];
$this->params['breadcrumbs'][] = ['label' => $model->subject->name, 'url' => ['student-mark/view', 'id'=>$model->student_id,
														'subject_id'=> $model->subject_id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="student-mark-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
