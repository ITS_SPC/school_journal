<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administrator\models\ParentProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Оценки учащегося по предмету';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-marks">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $studentFilterForm->filteredStudentsMarksDataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
          
            'mark',
            'mark_date',
            
            [
            'class' => 'yii\grid\ActionColumn',
			],

			],
    ]); ?>

</div>