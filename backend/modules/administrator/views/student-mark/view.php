<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\modules\administrator\models\Subject;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\StudentMark */

$this->title = $student->personalNumber->surname." ".$student->personalNumber->name." ".$student->personalNumber->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Оценки учащихся', 
    'url' => ['student-mark/index','school_year_id'=>$student->schoolClass->school->school_year_id,
                'school_id'=>$student->schoolClass->school->school_id,
                'school_class_id'=>$student->school_class_id]];
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = $model[0]->subject->name;
?>
<div class="student-mark-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <?php $form = ActiveForm::begin([
        'method' => 'GET',
        'action' => ['student-mark/view','id'=>$student->id]
        ]); ?>
     
        <div class="col-md-3">
            
            <?= $form->field($subject, 'id')->dropDownList(ArrayHelper::map(Subject::find()->all(), 'id', 'name'), 
                                                            array('prompt'=>'---Предмет---')) ?>

        </div>

    
        <div class="col-md-12">

            <p>
                <?= Html::submitButton('Найти', ['class' => 'btn btn-success']) ?>
            </p>
         
        </div>

    <?php ActiveForm::end(); ?>
  
</div>

<div class="row">
    <div class="col-md-12">    
    <?php
/*    print_r("<pre>"); 
    print_r($model); 
    print_r("</pre>"); */
   if($dataProvider){ ?>
        
          <?php if(isset($_GET[Subject][id])){
            $id_of_subject=$_GET[Subject][id];
        }else{
            $id_of_subject=\Yii::$app->getRequest()->getQueryParam('subject_id');
        } ?>



        <p>
        <?= Html::a('Добавить оценку', ['create', 'student_id'=> \Yii::$app->getRequest()->getQueryParam('id'), 
            'subject_id'=> $id_of_subject], 
                ['class' => 'btn btn-success']) ?>
        </p>

         <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                
                'mark',
                'mark_date',
            
                ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                ],
            ],

        ]); ?>
   <?php }
    ?>
</div>
</div>

