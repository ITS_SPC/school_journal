<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SubscriptionRate */

$this->title = 'Обновить тариф';
$this->params['breadcrumbs'][] = ['label' => 'Тарифы на подписку', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="subscription-rate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
