<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administrator\models\SubscriptionRateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тарифы на подписку';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-rate-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить тариф', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'value',
			[
			'attribute' => 'value',
			'label' => 'Величина тарифа',
			'value' => function ($model, $key, $index, $column) {
							return $model->value.'&#8381';
						},
			'format'=>'html',
			],

            ['class' => 'yii\grid\ActionColumn',
			'template'=>'{update} {delete}',
			],
        ],
    ]); ?>

</div>
