<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\administrator\models\Manager;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Школы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить школу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'phone',
            'address',
            //'manager_id',
			[
			'attribute' => 'manager_id',
			'label' => 'Фамилия менеджера',
			//'filter' => ArrayHelper::map(Manager::find()->asArray()->all(), 'id', 'surname'),
			'value' => function($model){
                return $model->manager->surname." "
                    .$model->manager->name." "
                    .$model->manager->lastname;
            }
			],
			[
			'attribute' => 'manager_id',
			'label' => 'Телефон менеджера',
			//'filter' => ArrayHelper::map(Manager::find()->asArray()->all(), 'id', 'phone'),
			'value' => 'manager.phone',
			],

            ['class' => 'yii\grid\ActionColumn',
			'controller' => 'school',
			'template' => '{update} {delete}',
			],
        ],
    ]); ?>

</div>
