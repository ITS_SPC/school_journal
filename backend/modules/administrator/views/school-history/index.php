<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use app\modules\administrator\models\School;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SubscriptionRate;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administrator\models\SchoolHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Школы в учебном году';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-history-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить школу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
			//'school_id',
            //'school_year_id',
            //'subscription_rate_id',
			[
			'attribute' => 'school_id',
			'label' => 'Школа',
			'filter' => ArrayHelper::map(School::find()->asArray()->all(), 'id', 'name'),
			'value' => 'school.name',
			],
			[
			'attribute' => 'school_year_id',
			'label' => 'Учебный год',
			'filter' => ArrayHelper::map(SchoolYear::find()->all(), 'id', function($model){
					return $model->start_year."/".$model->end_year;
				}
			),
			'value' => function($model){
					return $model->schoolYear->start_year."/"
						.$model->schoolYear->end_year;
				},
			],	
			[
			'attribute' => 'subscription_rate_id',
			'label' => 'Тариф',
			'filter' => ArrayHelper::map(SubscriptionRate::find()->asArray()->all(), 'id', 'value'),
			'value' => 'subscriptionRate.value',
			],	

            ['class' => 'yii\grid\ActionColumn',
			'template'=>'{update} {delete}',
			],
        ],
    ]); ?>

</div>
