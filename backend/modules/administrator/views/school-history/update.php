<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SchoolHistory */

$this->title = 'Обновить школу';
$this->params['breadcrumbs'][] = ['label' => 'Школы в учебном году', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="school-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
