<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\administrator\models\School;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SubscriptionRate;
/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SchoolHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="school-history-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'school_id')->dropDownList(ArrayHelper::map(School::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'school_year_id')->dropDownList(ArrayHelper::map(SchoolYear::find()->all(), 'id', function($model, $defaultValue) {
                return $model->start_year."/".$model->end_year;
            }), ['prompt' => '---Учебный год---']) ?>

    <?= $form->field($model, 'subscription_rate_id')->dropDownList(ArrayHelper::map(SubscriptionRate::find()->all(), 'id', 'value')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
