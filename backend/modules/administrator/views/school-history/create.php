<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SchoolHistory */

$this->title = 'Добавить школу';
$this->params['breadcrumbs'][] = ['label' => 'Школы в учебном году', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
