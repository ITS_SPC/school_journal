<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\administrator\models\SchoolHistory;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SchoolClass */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="school-class-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	 <?= $form->field($model, 'school_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(SchoolHistory::find()->all(), 'id', function($model){
	 	return $model->school->name.": ".$model->schoolYear->start_year."/".$model->schoolYear->end_year;
	 }),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите школу...'],
            'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
