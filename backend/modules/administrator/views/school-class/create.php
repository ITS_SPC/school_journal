<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\SchoolClass */

$this->title = 'Добавить класс';
$this->params['breadcrumbs'][] = ['label' => 'Классы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-class-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
