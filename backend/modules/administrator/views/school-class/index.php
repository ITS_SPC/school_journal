<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\administrator\models\School;
use app\modules\administrator\models\SchoolYear;
use app\modules\administrator\models\SchoolClass;
use app\modules\administrator\models\SchoolHistory;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\administrator\models\SchoolClassSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Классы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-class-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить класс', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            //'school_id',
            [
            'attribute'=> 'school_id',
            'label' => 'Школа',
            'filter'=> ArrayHelper::map(SchoolHistory::find()->all(), 'id', function($model){
                return $model->school->name.": "
                    .$model->schoolYear->start_year."/"
                    .$model->schoolYear->end_year;
            }),
            'value' => function($model){
                return $model->school->school->name.": "
                    .$model->school->schoolYear->start_year."/"
                    .$model->school->schoolYear->end_year;
            }
            ],

            ['class' => 'yii\grid\ActionColumn',
			'template'=>'{update} {delete}',
			],
        ],
    ]); ?>

</div>
