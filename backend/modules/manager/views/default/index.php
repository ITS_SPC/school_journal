<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Главная таблица менеджера: '.$school_year->start_year.'/'.$school_year->end_year.'';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manager-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'school_year'=>$school_year,
		])
		//print_r('<pre>');print_r($school_year);print_r('</pre>');
	?>

</div>
