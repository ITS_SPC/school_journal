<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\ManagerMainTableWidget;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use app\modules\administrator\models\SchoolYear;


?>

<div class="manager-profile-form">
	
	<!-- school_year choosing -->
	<?php $form = ActiveForm::begin(); ?>
	
	 <div class="row text-left">
		<div class="col-md-4">
    <?= $form->field($school_year, 'id')->dropDownList(ArrayHelper::map(SchoolYear::find()->all(), 'id', function($model, $defaultValue) {
                return $model->start_year."/".$model->end_year;
            }), ['prompt' => '---Учебные года---'])?>					
		</div>
	</div>

	<div class="row text-left">
        <div class="col-md-12">
            <?= Html::submitButton('Показать статистику', ['class' => 'btn btn-success' ]) ?>   
        </div>
    </div>
	<br>
		
    <?php ActiveForm::end(); ?>

		<!--end -->
	
    <?= ManagerMainTableWidget::widget([
            'model' => $model,
			'school_year'=>$school_year,
        ]);
    ?>

    <!-- Modal window for chart statistic -->
    <?php
      Modal::begin([
        'header' => '<h2 class="modal-title"></h2>',
        'id' => 'chartModal'
      ]);
    ?>
    	<div class="row">
    		<div class="col-md-6">
    			<canvas id="myChart" width="300px" height="300px"></canvas>
    		</div>
    		<div class="col-md-6">
    			<div id="chart-legend"></div>
    		</div>
    	</div>
    <?php
      Modal::end();
    ?>

</div>