<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мой профиль';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="manager-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="row">
		<div class="col-md-6">

			<?= $this->render('_profile', [
        		'profile_model' => $profile_model,
    		]) ?>	

		</div>
		<div class="col-md-6">
			
			<?= $this->render('_user', [
        		'user' => $user,
    		]) ?>

		</div>
	</div>
</div>
