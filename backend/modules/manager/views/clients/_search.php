<?php

use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\SchoolSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-search">
	<div class="panel panel-primary">
  		<div class="panel-heading">
  			<h3>Поиск клиентов</h3>
  		</div>
		<div class="panel-body">

			<?php $form = ActiveForm::begin([
        		'action' => ['index'],
        		'method' => 'get',
    		]); ?>
    			<div class="row">
    				<div class="col-md-2">
    						
    					<?= $form->field($searchModel, 'school_id')->widget(Select2::classname(), [
					            'data' => ArrayHelper::map($searchModel->schools, 'id', function($model){
					    			return $model->name;
					   			}),
					            'language' => 'ru',
					            'options' => ['placeholder' => 'Выберите школу...'],
					            'pluginOptions' => [
					            'allowClear' => true
					        ],
			    		]); ?>

    				</div>
    				<div class="col-md-2">
    					
    					<?= $form->field($searchModel, 'surname') ?>

    				</div>
    				<div class="col-md-2">
    					
    					<?= $form->field($searchModel, 'name') ?>

    				</div>
    				<div class="col-md-2">
    					
    					<?= $form->field($searchModel, 'lastname') ?>

    				</div>
    				<div class="col-md-2">

    					<?= $form->field($searchModel, 'address') ?>
    					
    				</div>
    				<div class="col-md-2">
    					<div class="form-group" style="margin: 25px 0 0 0">

					        <?= Html::submitButton('Найти', ['class' => 'btn btn-success']) ?>
					        <?= Html::resetButton('Сбросить', ['class' => 'btn btn-default']) ?>

			    		</div>
    				</div>
    			</div>

    		<?php ActiveForm::end(); ?>

		</div>
</div>

    <?php if (isset($dataProvider)) { ?>

			<?=  GridView::widget([
			    'dataProvider' => $dataProvider,
			    'filterModel' => $searchModel,
			    'columns' => [
			    	['class' => 'yii\grid\SerialColumn'],
			    	'surname',
			    	'name',
			    	'lastname',
			    	'address',
			    	[
			    		'class' => 'yii\grid\ActionColumn',
			    		'template' => '{view}',
			    	],
			    ],
			]); ?>

    <?php } ?>

</div>
