<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\School */

$this->title = $parentProfile->surname." ".$parentProfile->name." ".$parentProfile->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $parentProfile,
        'attributes' => [
            'name',
            'surname',
            'lastname',
            'address'
        ],
    ]) ?>

    <h3>Телефоны</h3>

    <?=  GridView::widget([
        'dataProvider' => $parentPhoneDataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'phone_number',
        ],
    ]); ?>
    

</div>
