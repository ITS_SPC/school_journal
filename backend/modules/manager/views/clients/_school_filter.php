<?php

use kartik\select2\Select2;
use app\modules\manager\models\School;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\administrator\models\ParentStudent */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin([
        'method' => 'get',
    ]); ?>
  	<?= $form->field($schoolFilterForm, 'school_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(School::find()->all(), 'id', function($model){
    return $model->name;
   }),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите школу...'],
            'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
    <?= Html::submitButton('Найти', ['class' => 'btn btn-success' ]) ?>
<?php ActiveForm::end(); ?>
<?php if (isset($schoolFilterForm->parentsDataProvider)) { ?>

<?=  GridView::widget([
                'dataProvider' => $schoolFilterForm->parentsDataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                        'surname',
                        'name',
                        'lastname',
                        'address',
                    [
                    //'attribute'=>'student_id',
                    /*'label'=>'Учащиеся школы',
                    'value'=> function($model){
                        return $model->personalNumber->surname." "
                            .$model->personalNumber->name." "
                            .$model->personalNumber->lastname;
                    }*/
                    ],
                    [ 
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    ],
                ],
            ]); ?>
    <?php } ?>

