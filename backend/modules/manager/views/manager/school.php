<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'ФИО и подписки';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="manager-school">
	<h1><?= Html::encode($this->title) ?></h1>
	
	<?=  GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           ['class' => 'yii\grid\SerialColumn'],
			[	
				'class' =>'yii\grid\dataColumn',
				'format'=>'raw',
				'attribute'=>'ФИО',
				'label'=>'ФИО',
				'value'=> function($model){
					return Html::a($model['ФИО'],['manager/school']
					); 
				},
			],
			'Количество_месяцев_подписки',
        ],
    ]);
	?> 
		
</div>