<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\ManagerMainTableWidget;
use yii\helpers\ArrayHelper;

?>

<div class="manager-profile-form">
	
	<?php ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'rate')->dropDownList([
		'0' => 'Активный',
		'1' => 'Отключен',
		'2'=>'Удален'
		]); 
	?>
	
    <?= ManagerMainTableWidget::widget([
            'model' => $model,
        ]);
    ?>
    <?php ActiveForm::end(); ?>

</div>