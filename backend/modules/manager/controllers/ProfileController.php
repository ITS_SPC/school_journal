<?php

namespace app\modules\manager\controllers;

use Yii;
use app\modules\manager\models\Manager;
use app\modules\manager\models\User;
use app\modules\manager\controllers\DefaultController;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProfileController implements the CRUD actions for Manager model.
 */
class ProfileController extends DefaultController {

    /**
     * Parent profile data.
     * @return mixed
     */
    public function actionIndex() {
        //get current user profile
        $profile_model = $this->findModel(Yii::$app->user->identity->id);
        $user = User::find()
            ->where(['id' => Yii::$app->user->identity->id])
            ->one();

        return $this->render('index', [
            'profile_model' => $profile_model,
            'user' => $user
        ]);
    }

    /**
     * Finds the ParentProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParentProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Manager::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
