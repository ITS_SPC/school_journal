<?php

namespace app\modules\manager\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use yii\grid\GridView;
use app\modules\administrator\models\SchoolYear;
use common\models\ContactForm;
use common\models\AuthAssignment;

class DefaultController extends Controller {
	
	public function behaviors() {
		return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                    	'allow' => true,
                    	'roles' => ['manager'],
                    ],
                ],
            ],
        ];
	}


	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	public function actionIndex() {
		if (isset($_POST['SchoolYear']['id'])) {
			$school_year = $this->findSchoolYearById($_POST['SchoolYear']['id']);
			$begin = $school_year->start_year;
			$end = $school_year->end_year;
		} else {
			$school_year=new SchoolYear();
			if (date('m')<9) {
				$school_year->start_year=(date("Y")-1);
				$begin=$school_year->start_year;
				$school_year->end_year=(date("Y"));
				$end=$school_year->end_year=(date("Y"));
			} else {
				$school_year->start_year=(date("Y"));
				$begin=$school_year->start_year;
				$school_year->end_year=(date("Y")+1);
				$end=$school_year->end_year=(date("Y"));
			}
		}
		$manager_id = Yii::$app->user->identity->id;
		$count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM school WHERE manager_id=:manager_id', [':manager_id' => $manager_id])->queryScalar();
	
		$dataProvider = new SqlDataProvider([
			
			'sql' => 'SELECT school.id AS id, school.name AS School, parent_subscription.subscription_start AS 
				BEGIN , parent_subscription.subscription_end AS 
				END , subscription_rate.value AS rate
				FROM school
				JOIN school_history ON school.id = school_history.school_id
				JOIN school_class ON school_history.id = school_class.school_id
				JOIN subscription_rate ON school_history.subscription_rate_id = subscription_rate.id
				JOIN school_student ON school_class.id = school_student.school_class_id
				JOIN student ON school_student.personal_number = student.id
				JOIN parent_student ON student.id = parent_student.student_id
				JOIN parent_subscription ON parent_student.id = parent_subscription.parent_student_id
				WHERE school.manager_id = :managerId && (parent_subscription.subscription_start
				BETWEEN CONCAT(:beginYear, "-09", "-01") AND CONCAT(:endYear, "-05", "-31"))
				group by parent_subscription.id',
			'params' => [
				':managerId' => $manager_id,
				':beginYear' => $begin,
				':endYear' => $end
			],
		    'totalCount' => (int)$count,
		]);
		$model=$dataProvider->getModels();
	
		return $this->render('index',[
			'dataProvider'=>$dataProvider,
			'model' => $model,
			'school_year'=>$school_year,
		]);
    }
	
	public function actionSchool() {
		if (isset($_POST['id'])) {
			$id = $_POST['id'];
			$manager_id = Yii::$app->user->identity->id;
			$count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM school WHERE manager_id=:manager_id', [':manager_id' => $manager_id])->queryScalar();
			if (isset($_POST['schoolYearId'])) {
				$schoolYearId = $_POST['schoolYearId'];
				$schoolYear = SchoolYear::find()
					->where(['id' => $schoolYearId])
					->one();
			} else {
				$schoolYear = new SchoolYear();
				if(date('m')<9){
					$schoolYear->start_year=(date("Y")-1);
					$schoolYear->end_year=(date("Y"));
				} else {
					$schoolYear->start_year=(date("Y"));
					$schoolYear->end_year=(date("Y")+1);
				}
			}
			$dataProvider = new SqlDataProvider([
		
				'sql' => 'SELECT school.id AS id, school.name AS School, parent_subscription.subscription_start AS 
					BEGIN , parent_subscription.subscription_end AS 
					END , subscription_rate.value AS rate
					FROM school
					JOIN school_history ON school.id = school_history.school_id
					JOIN school_class ON school_history.id = school_class.school_id
					JOIN subscription_rate ON school_history.subscription_rate_id = subscription_rate.id
					JOIN school_student ON school_class.id = school_student.school_class_id
					JOIN student ON school_student.personal_number = student.id
					JOIN parent_student ON student.id = parent_student.student_id
					JOIN parent_subscription ON parent_student.id = parent_subscription.parent_student_id
					WHERE school.manager_id = :managerId && (parent_subscription.subscription_start
					BETWEEN CONCAT(:beginYear, "-09", "-01") AND CONCAT(:endYear, "-05", "-31")) && (school.id = :schoolId)
					group by parent_subscription.id',
				'params' => [
					':managerId' => $manager_id,
					':beginYear' => $schoolYear->start_year,
					':endYear' => $schoolYear->end_year,
					':schoolId' => $id
				],
	    		'totalCount' => (int)$count,
			]);
	
			$model = $dataProvider->getModels();

			$count;
			$calendar = array('09' => 0,'10' => 0,'11' => 0,'12' => 0,'01' => 0,'02' => 0,'03' => 0,'04' => 0,'05' => 0);
			$tmp = array_keys($calendar);
			foreach($tmp as $k) {
				if((int)$k >= 9){
					$year=$schoolYear->start_year;
				} else {
					$year=$schoolYear->end_year;
				}
				$count=0;
				foreach($model as $v) {
					if($v[BEGIN]< ''.$year.'-'.$k.'-15' and ''.$year.'-'.$k.'-15' < $v[END]){
						$count++;
					} 
				}
				$calendar[$k] = $count * $model[0][rate];
			}
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return json_encode($calendar);
 		} else {
			return null;
		}
	}
	
	protected function findSchoolYearById($id)
    {
		$school_year = SchoolYear::find()->where(['id'=>$id])->one();
			return $school_year;
    }

    public function actionContact()
    {   
        $role_name=AuthAssignment::find()->where(['user_id'=>Yii::$app->user->identity->id])->one()->item_name;

        $model = new ContactForm();

        $model->role_name=$role_name;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Спасибо за обращение к нам. Мы ответим как можно быстрее.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке сообщения.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

}
