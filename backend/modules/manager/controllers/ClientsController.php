<?php

namespace app\modules\manager\controllers;

use Yii;
use app\modules\manager\models\Manager;
use app\modules\manager\models\User;
use app\modules\manager\models\ParentProfileSearch;
use app\modules\manager\models\ParentProfile;
use app\modules\manager\models\ParentPhone;
use app\modules\manager\controllers\DefaultController;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientsController implements actions for Manager model.
 */
class ClientsController extends DefaultController {

    /**
     * Parent profile data.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ParentProfileSearch(['managerId' => Yii::$app->user->identity->id]);

        if ($searchModel->load(Yii::$app->request->get()) && $searchModel->validate()) {
        	$dataProvider = $searchModel->search(Yii::$app->user->identity->id, Yii::$app->request->queryParams);
        }

        return $this->render('index', [
        	'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
    * Parent data
    */
    public function actionView($id) {
    	$parentProfile = $this->findParentProfile($id);
    	$parentPhoneDataProvider = new ActiveDataProvider([
            'query' => ParentPhone::find()
            	->where(['parent_id' => $id]),
            'pagination' => [
               'pageSize' => 50,
            ],
        ]);

    	return $this->render('view', [
    		'parentProfile' => $parentProfile,
    		'parentPhoneDataProvider' => $parentPhoneDataProvider
        ]);
    }

    /**
     * Finds the ParentProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ParentProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Manager::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findParentProfile($id) {
    	if (($model = ParentProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Профиль родителя пуст. Обратитесь к администратору.');
        }
    }
}
