<?php

namespace app\modules\manager\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\manager\models\ParentProfile;
use app\modules\manager\models\School;

/**
 * ParentProfileSearch represents the model behind the search form about `app\modules\manager\models\ParentProfile`.
 */
class ParentProfileSearch extends ParentProfile
{
    public $school_id;
    public $name;
    public $lastname;
    public $surname;
    public $address;

    public $schools;

    function __construct ( $config = [] ) {
        $this->getManagerSchools($config['managerId']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['id', 'manager_id'], 'integer'],
            [['name', 'surname', 'lastname', 'address', 'school_id'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'school_id' => 'Школа',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'lastname' => 'Отчество',
            'address' => 'Адрес'
        ];
    }

    public function getManagerSchools($managerId = null) {
        $this->schools = School::find()
            ->where(['manager_id' => $managerId])
            ->all();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($managerId = null, $params)
    {
        $this->load($params);
        $query = ParentProfile::find()
            ->leftJoin('parent_student', '`parent_student`.`parent_id` = `parent`.`id`')
            ->leftJoin('student', '`student`.`id` = `parent_student`.`student_id`')
            ->leftJoin('school_student', '`school_student`.`personal_number` = `student`.`id`')
            ->leftJoin('school_class', '`school_student`.`school_class_id` = `school_class`.`id`')
            ->leftJoin('school_history', '`school_class`.`school_id` = `school_history`.`id`')
            ->leftJoin('school', '`school_history`.`school_id` = `school`.`id`')
            ->where('school.id = :schoolId AND school.manager_id = :managerId',
                [
                    'schoolId' => $this->school_id,
                    'managerId' => $managerId
                ])
            ->distinct();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
               'pageSize' => 50,
            ],
        ]);

        //$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'parent.name', $this->name])
            ->andFilterWhere(['like', 'parent.surname', $this->surname])
            ->andFilterWhere(['like', 'parent.lastname', $this->lastname])
            ->andFilterWhere(['like', 'parent.address', $this->address]);

        return $dataProvider;
    }
}
