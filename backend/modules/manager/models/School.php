<?php

namespace app\modules\manager\models;

use Yii;

/**
 * This is the model class for table "school".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $address
 * @property integer $school_year_id
 * @property integer $manager_id
 *
 * @property SchoolYear $schoolYear
 * @property Manager $manager
 * @property SchoolClass[] $schoolClasses
 */
class School extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manager_id'], 'integer'],

            [['name', 'phone', 'address'], 'required'],
            [['name', 'phone', 'address'], 'filter', 'filter' => 'trim'],
            [['name', 'phone', 'address'], 'string', 'min' => 2, 'max' => 200],

            ['phone', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'manager_id' => 'Менеджер',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Manager::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolClasses()
    {
        return $this->hasMany(SchoolClass::className(), ['school_id' => 'id']);
    }

}
