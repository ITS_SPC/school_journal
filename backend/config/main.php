<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'administrator' => [
            'class' => 'app\modules\administrator\Module',
            'layout' => 'main',
        ],
        'manager' => [
            'class' => 'app\modules\manager\Module',
            'layout' => 'main',
        ],
        'worker' => [
            'class' => 'app\modules\worker\Module',
            'layout' => 'main',
        ],
        'parent' => [
            'class' => 'app\modules\parent\Module',
            'layout' => 'main',
        ],
    ],
    'components' => [
        'robokassa' => [
            'class' => '\robokassa\Merchant',
            'baseUrl' => 'http://auth.robokassa.ru/Merchant/Index.aspx',
            'sMerchantLogin' => 'mobilejournal',
            'sMerchantPass1' => 'CZmrfT1ZP5UtRgVR8u02',
            'sMerchantPass2' => 'OzDeH1YV1L86ek4rDCte',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            /*'rules' => array(
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>', 
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),*/
        ],
    ],
    'params' => $params,
];
