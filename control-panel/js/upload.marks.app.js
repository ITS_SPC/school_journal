angular.module('uploadMarksApp', []);
angular.module('uploadMarksApp').controller('UploadForm', function($scope, $http) {
	
	$scope.schools = [];
	$scope.selectedSchool = {};

	$scope.schoolClasses = [];
	$scope.selectedClass = {};

	$scope.$watch('schoolYearId', function(newVal, oldVal) {
		if (newVal !== oldVal) {
			$('.spinner').show();
			$scope.getSchools();
		}
	});

	$scope.$watch('selectedSchool.id', function(newVal, oldVal) {
		if (newVal !== oldVal) {
			$('.spinner').show();
			$scope.getClasses();
		}
	})

	$scope.getClasses = function() {
		$http.post('get-classes', {
			data: {
				schoolYearId: $scope.schoolYearId,
				schoolId: $scope.selectedSchool.id
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(rateObject) {
			$scope.schoolClasses = rateObject;
		}).error(function(error) {
			alert("Ошибка сервера");
		}).finally(function() {
			$('.spinner').hide();
		});
	};

	$scope.getSchools = function() {
		//clear classes
		$scope.schoolClasses = [];
		$scope.selectedClass = {};
		$http.post('get-schools', {
			data: {
				schoolYearId: $scope.schoolYearId,
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(rateObject) {
			$scope.schools = rateObject;
			console.log(rateObject);
		}).error(function(error) {
			alert("Ошибка сервера");
		}).finally(function() {
			$('.spinner').hide();
		});
	};
});