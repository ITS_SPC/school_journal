(function() {

	//school class
	function SchoolModel(schoolName, id, subscriptionData, schoolYearId) {
		this.schoolName = schoolName;
		this.id = id;
		this.subscriptionData = subscriptionData;
		this.schoolYearId = schoolYearId;
		return this;
	}
	SchoolModel.prototype.getKeyValuesFromSubscriptionData = function() {
		var values = [];
		if (this.subscriptionData) {
			for(var key in this.subscriptionData) {
				values.push(key);
			}
		}
		return values.sort();
	};
	SchoolModel.prototype.sortMonthsByDecade = function(months) {
		var firstDecade = [],
			secondDecade = [];
		if (months.length) {
			for (var i = 0, maxi = months.length; i < maxi; i++) {
				if ((months[i] <= 12) && (months[i] >= 9)) {
					firstDecade.push(months[i]);
				} else {
					secondDecade.push(months[i]);
				}
			}
			return firstDecade.sort().concat(secondDecade.sort());
		} else {
			return months; 
		}
	};
	SchoolModel.prototype.getMonthRates = function(months) {
		var monthsRate = [];
		if (months.length) {
			for (var i = 0, maxi = months.length; i < maxi; i++) {
				monthsRate.push(this.subscriptionData[months[i]]);
				months[i] = this.getMonthWord(months[i]);
			} 
		}
		return monthsRate;
	};
	SchoolModel.prototype.getMonthWord = function(month) {
		switch(month) {
			case '09': {
				return 'Сен';
			} break;
			case '10': {
				return 'Окт';
			} break;
			case '11': {
				return 'Ноя';
			} break;
			case '12': {
				return 'Дек';
			} break;
			case '01': {
				return 'Янв';
			} break;
			case '02': {
				return 'Фев';
			} break;
			case '03': {
				return 'Мар';
			} break;
			case '04': {
				return 'Апр';
			} break;
			case '05': {
				return 'Май';
			} break;
		}
	};

	//get data and create chart
	function createChart() {

		var school = new SchoolModel($(this).text(), $(this).attr('id'), null, $(this).data('year')),
			chartData = {},
			ctx = document.getElementById("myChart").getContext("2d"),
			schoolChartStatistic;

		$.post( "manager/default/school", { id: school.id, schoolYearId: school.schoolYearId })
  			.done(function(data) {
  				school.subscriptionData = JSON.parse(data);
  				chartData.labels = school.sortMonthsByDecade(school.getKeyValuesFromSubscriptionData());
  				chartData.datasets = [
  					{
            			label: "График оформленных подписок школы",
            			fillColor: "rgba(151,187,205,0.2)",
            			strokeColor: "rgba(151,187,205,1)",
            			pointColor: "rgba(151,187,205,1)",
            			pointStrokeColor: "#fff",
            			pointHighlightFill: "#fff",
            			pointHighlightStroke: "rgba(151,187,205,1)",
            			data: school.getMonthRates(chartData.labels)
        			}
  				];
  				
  				
  				$('.modal-title').text(school.schoolName);
  				schoolChartStatistic = new Chart(ctx).Line(chartData, {
  					 legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
  				});
  				document.getElementById("chart-legend").innerHTML = schoolChartStatistic.generateLegend();
  				$('#chartModal').modal();
  			})
  			.fail(function() {
    			alert("Ошибка при получении данных!");
  			});
	 }

	//disable search button
	function checkListSelection() {
		($(this).val()) ? ($("button").prop('disabled', false)) : ($("button").prop('disabled', true));
	}

	//init module
	$('.school-link').on('click', createChart);
	$('#schoolyear-id').change(checkListSelection);
	$('#schoolyear-id').trigger('change');
})();