angular.module('studentOrderApp', []);
angular.module('studentOrderApp').controller('OrderController', function($scope, $http){
	$scope.subjectStudents = [];
	$scope.classStudents = [];
	$scope.alerts = {};
	
	$scope.assembleData = function() {
		$scope.subjectStudents = [];
		$scope.classStudents = [];
		$scope.subjectStudents = $scope.getStudents([angular.element(document.querySelector('#subjectStudents')).children()]);
		$scope.classStudents = $scope.getStudents([angular.element(document.querySelector('#classStudents')).children(), angular.element(document.querySelector('#subjectStudents')).children()]);
		if ($scope.classStudents.length) {
			$scope.setOrder();
		}
	};

	$scope.getStudents = function(containers) {
		var students = [];
		if (containers.length) {
			for (var i = 0, maxi = containers.length; i < maxi; i++) {
				angular.forEach(containers[i], function(value, key){
     				var a = angular.element(value);
     				students.push({
     					student: a.attr('id'),
     					subject: a.attr('data-subject'),
     					order: key + 1 
     				});
				});
			}
		}
		return students;
	};

	$scope.setOrder = function() {
		$('.saving-order').show();
		$scope.clearAlerts();
		$http.post('set-order', {
			data: {
				classStudents: $scope.classStudents,
				subjectStudents: $scope.subjectStudents
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(rateObject) {
			$scope.addAlert('Порядок успешно изменен', 'alert-success');
		}).error(function(error) {
			$scope.addAlert('Ошибка сервера', 'alert-danger');
		}).finally(function() {
			$('.saving-order').hide();
		});
	};

	$scope.moveStudentsToSubject = function() {
		angular.element(document.querySelector('#classStudents')).children().appendTo(angular.element(document.querySelector('#subjectStudents')));
	};

	$scope.removeStudentsFromSubject = function() {
		angular.element(document.querySelector('#subjectStudents')).children().appendTo(angular.element(document.querySelector('#classStudents')));
	};

	$scope.addAlert = function(message, type) {
		$scope.alerts[type] = $scope.alerts[type]||[];
		$scope.alerts[type].push(message);
	};

	$scope.clearAlerts = function() {
		for(var x in $scope.alerts) {
           delete $scope.alerts[x];
        }
	};
});

//bootstraping module
angular.bootstrap(document.getElementById("studentOrderApp"), ['studentOrderApp']);