var subscribeApp = angular.module('subscribeApp', ['ui.bootstrap']);
subscribeApp.controller('SubscriptionFormController', function($scope, $http) {
	$scope.subscriptionForm = {};
	$scope.$watchCollection('subscriptionForm', function (newVals, oldVals) {
		//if (newVals.studentId && newVals.phoneId && newVals.schoolYearId && newVals.subscriptionStart && newVals.subscriptionEnd) {
			console.log(newVals);
		//}
		/*if (newVals.studentId && newVals.phoneId && newVals.schoolYearId && newVals.subscriptionStart && newVals.subscriptionEnd) {

			if (!newVals.monthRate || (newVals.studentId !== oldVals.studentId) || (newVals.schoolYearId !== oldVals.schoolYearId) ) {
				$scope.getSchoolYearRate();
			}
			if (!Number(newVals.monthRate)) {
				alert("Ученика нет в учебном году");
			}
		}*/
	});
	$scope.getSchoolYearRate = function() {
		$http.post('rate', {
			data: {
				studentId: $scope.subscriptionForm.studentId,
				schoolYearId: $scope.subscriptionForm.schoolYearId
			},
			_csrf: yii.getCsrfToken(),
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
		}).success(function(rateObject) {
			if (rateObject) {
				$scope.subscriptionForm.monthRate = rateObject.value;
			} else {
				$scope.subscriptionForm.monthRate = 'Учащегося нет в заданном учебном году';
			}
			console.log($scope.subscriptionForm);
		}).error(function(error) {
			console.log(error);
		});
	}
});