<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use frontend\components\PartnerWidget;
use frontend\components\SchoolWidget;
use frontend\components\VideoWidget;
use yii\widgets\ListView;

$this->title = 'Школьный журнал';
?>

<div id="fullpage">
	<div class="section" id="section1">
		<h1>Добро пожаловать</h1>
	<?
		echo VideoWidget::widget(['model'=>$video]);
	?>
	</div>

	<div class="section" id="section2">
		<h1>Партнеры</h1>
	<?php
		echo PartnerWidget::widget(['model'=> $partner]);
	?>
	</div>
	
	<div class="section" id="section3">
		<h1>Школы</h1>
	<?php
		echo SchoolWidget::widget(['model'=> $school]);
	?>
	</div>

</div>
