<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="post">   
<?php 
	if(count($model)<6){
		echo('<table cellspacing="10" cellpadding="10" width="100%">');
			$counter=0;
			for($i=0;$i<(count($model));$i++){
				$counter++;
				if($model[$i][logo]==""){
					echo('<td width="33%" height="250px"><br><h4>'.HtmlPurifier::process($model[$i][name]).'</h4></td>');
				}else{
					echo('<td width="33%">'.Html::img('frontend/uploads/'.$model[$i][logo], ['style' => "width:150px"]).'<br><h4>'
					.HtmlPurifier::process($model[$i][name]).'</h4><br></td>');
					if($counter==3){
						echo('<tr><br>');
						$counter=0;
					}
				}
			}
		echo('</table>');
	}else{
		$i=0;
		for($j=1;$j<((ceil((count($model))/6))+1);$j++){
			echo('<div class="slide" id="slide">');
			$counter=0;
			echo('<table cellspacing="10" cellpadding="10" width="100%">');
			for(;$i<(6*$j);$i++){
				$counter++;
				if($model[$i][logo]==""){
					echo('<td width="33%" height="250px"><br><h4>'.HtmlPurifier::process($model[$i][name]).'</h4></td>');
				}
				else{
					echo('<td width="33%">'.Html::img('frontend/uploads/'.$model[$i][logo], ['style' => "width:150px"]).
					'<br><h4>'.HtmlPurifier::process($model[$i][name]).'</h4></td>'); 
				}
				if($counter==3){
					echo('<tr><br>');
					$counter=0;
				}	
			} 
			echo('</table></div>');
		}
	}
?>
</div>