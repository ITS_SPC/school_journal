<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="panel panel-primary">
        <div class="panel-heading text-center">
            <h1 style="padding:0"><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                        <?= $form->field($model, 'username') ?>

                        <?= $form->field($model, 'password')->passwordInput() ?>
                        <div class="row text-right">
                            <div class="col-md-12">
                                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                            </div>
                        </div>
                        <div style="color:#999;margin:1em 0" class="text-right">
                            Если Вы забыли пароль, Вы можете <?= Html::a('сбросить его', ['site/request-password-reset']) ?>.
                        </div>

                        <div class="form-group text-right">
                            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
