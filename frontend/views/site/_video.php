<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="post">   
<?php 
	if(count($model)==1){
		echo('<table cellspacing="10" cellpadding="10" width="100%"  border="0px">');
			$counter=0;
			for($i=0;$i<(count($model));$i++){
				$counter++;
				echo('<td width="33%"><video style="width: 100%; height:100%" src="../frontend/uploads/'.$model[$i][name].'" controls autoplay></video>');
				echo('<br><h4>'.HtmlPurifier::process($model[$i][description]).'</h4><br></td>');
				if($counter==3){
					echo('<tr><br>');
					$counter=0;
				}
			}
		echo('</table>');
	} else{
		for($i=0;$i<(count($model));$i++){
			echo('<div class="slide" id="slide">
					<video style="width: 100%; height:100%" src="../frontend/uploads/'.$model[$i][name].'" controls autoplay></video>
					<p>'.$model[$i][description].'</p></div>');
		}				
	}
?>
</div>