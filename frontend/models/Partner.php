<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "partner".
 *
 * @property integer $id
 * @property string $partnername
 * @property string $description
 * @property string $logo
 */
class Partner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	 
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partnername'], 'required'],
            [['description'], 'string'],
            [['partnername', 'logo'], 'string', 'max' => 255],
			[['publish'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'partnername' => Yii::t('app', 'Имя партнера'),
            'description' => Yii::t('app', 'Информация'),
            'logo' => Yii::t('app', 'Логотип'),
            'publish' => Yii::t('app', 'Опубликовано'),
        ];
    }
}
