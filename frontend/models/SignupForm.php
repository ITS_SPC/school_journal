<?php
namespace frontend\models;

use common\models\User;
use common\models\ParentProfile;
use common\models\ParentPhone;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $passwordConfirm;
    public $name;
    public $surname;
    public $lastname;
    public $address;
    public $verifyCode;
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //user fields
            ['username', 'required'],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Логин уже занят.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['password', 'required'],
            ['password', 'filter', 'filter' => 'trim'],
            ['password', 'string', 'min' => 6],

            ['email', 'required'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'E-mail уже занят.'],

            ['passwordConfirm', 'required'],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password'],

            //profile fields
            [['surname', 'name', 'lastname', 'address'], 'required'],
            [['surname', 'name', 'lastname', 'address'], 'filter', 'filter' => 'trim'],
            [['surname', 'name', 'lastname', 'address'], 'string', 'min' => 2, 'max' => 200],

            ['phone', 'required'],
            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'string', 'min' => 2, 'max' => 100],

            ['verifyCode', 'required'],
            ['verifyCode', 'captcha'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя пользователя(логин)',
            'email' => 'E-mail',
            'password' => 'Пароль',
            'passwordConfirm' => 'Повторите пароль',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'lastname' => 'Отчество',
            'address' => 'Адрес',
            'phone' => 'Моб.телефон',
            'verifyCode' => 'Код'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $userRole = Yii::$app->authManager->getRole('parent');
            if ($user->save()) {
                Yii::$app->authManager->assign($userRole, $user->getId());
                $parent = new ParentProfile();
                $parent->id = $user->id;
                $parent->name = $this->name;
                $parent->surname = $this->surname;
                $parent->lastname = $this->lastname;
                $parent->address = $this->address;
                if ($parent->save()) {
                    $parentPhone = new ParentPhone();
                    $parentPhone->phone_number = $this->phone;
                    $parentPhone->parent_id = $parent->id;
                    $parentPhone->save();
                    return $user;
                }
            }
        }

        return null;
    }
}
