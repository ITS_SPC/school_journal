<?php
namespace frontend\controllers;

use Yii;
use frontend\models\Video;
use frontend\models\Partner;
use frontend\models\SchoolPartner;
use frontend\models\Contact;
use yii\data\ActiveDataProvider;


use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                //'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		$videoDataProvider = new ActiveDataProvider([
		'query' => Video::find()->where(['publish'=>'1']),
		]);
		$video = $videoDataProvider->getModels();
		
		
		$partnerDataProvider = new ActiveDataProvider([
		'query' => Partner::find()->where(['publish'=>'1']),
		]);
		$partner = $partnerDataProvider->getModels();
		
		$schoolDataProvider = new ActiveDataProvider([
		'query' => SchoolPartner::find()->where(['publish'=>'1']),
		]);
		$school = $schoolDataProvider->getModels(); 
		
		
		return $this->render('index',[
			'videoDataProvider' => $videoDataProvider, 'video' => $video,
            'partnerDataProvider' => $partnerDataProvider, 'partner'=>$partner,
			'schoolDataProvider' => $schoolDataProvider, 'school'=>$school,
		]);
		
    }
	
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //redirect to user module
            foreach (Yii::$app->authManager->getRolesByUser(Yii::$app->user->ID) as $user_role) {
                switch ($user_role->name) {
                    case 'administrator': {
                        $this->redirect(Url::home().'control-panel/administrator');
                    }break;
                    case 'manager': {
                        $this->redirect(Url::home().'control-panel/manager');
                    }break;
                    case 'parent': {
                        $this->redirect(Url::home().'control-panel/parent');
                    }break;
                    case 'worker': {
                        $this->redirect(Url::home().'control-panel/worker');
                    }break;
                    /*default: {
                        return $this->goBack();
                    }break;*/
                }
            }
            //return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
		////added
		//$contact = new Contact();
		$contactDataProvider = new ActiveDataProvider([
		'query' => Contact::find(),
		]);
		$contact = $contactDataProvider->getModels();
		///////
		
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Спасибо за обращение к нам. Мы ответим как можно быстрее.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке сообщения.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
				'contact' => $contact,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
    	$model = new SignupForm();
    	if ($model->load(Yii::$app->request->post())) {
    		if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(Url::home().'control-panel');
                }
            }
    	}

    	return $this->render('signup', [
            'model' => $model,
        ]);
        /*$model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);*/
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }	
}
