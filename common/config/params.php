<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'websms' => ['login' => 'Arma_92', 'password' => '123456', 'logFilePath' => 'console/runtime/logs/sms.log'],
];
